﻿using AutoMapper;
using CinemaProject.Application.UseCases.Users;
using CinemaProject.Domain.Entities;
using CinemaProject.Domain.Relations;
using System.Linq;

namespace CinemaProject.Implementation.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, ReadUserDto>()
               .ForMember(dto => dto.Roles, o => o.MapFrom(u => u.UserRoles.Select(ur => ur.Role.Name)))
               .ForMember(dto => dto.UseCases, o => o.MapFrom(u =>
                    u.UserRoles.SelectMany(ur =>
                        ur.Role.RoleUseCases.Select(ruc =>
                            ruc.UseCaseId
                        )
                    ).Where(x =>
                        !u.UserUseCases
                            .Any(uuc => uuc.UseCaseId == x && !uuc.IsAllowed))
                            .Union(u.UserUseCases.Where(uuc => uuc.IsAllowed).Select(uuc => uuc.UseCaseId))
                    )
               );

            CreateMap<UserDto, User>()
                .ForMember(u => u.UserRoles, o =>
                    o.MapFrom(dto => dto.RoleIds.Select(r => new UserRole { RoleId = r }))
                )
                .ForMember(u => u.UserUseCases, o => o.MapFrom(dto =>
                    dto.UserSpecificUseCases.Select(usuc =>
                        new UserUseCase { UseCaseId = usuc.UseCaseId, IsAllowed = usuc.IsAllowed }))
                )
                .ForMember(u => u.Password, o =>
                    {
                        o.PreCondition(dto => !string.IsNullOrWhiteSpace(dto.Password));
                        o.MapFrom(dto => BCrypt.Net.BCrypt.HashPassword(dto.Password));
                    }
                );
        }
    }
}