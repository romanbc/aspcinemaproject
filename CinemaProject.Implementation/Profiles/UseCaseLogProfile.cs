﻿using AutoMapper;
using CinemaProject.Application.UseCases.UseCaseLogs;
using CinemaProject.Domain.Entities;

namespace CinemaProject.Implementation.Profiles
{
    public class UseCaseLogProfile : Profile
    {
        public UseCaseLogProfile()
        {
            CreateMap<UseCaseLog, UseCaseLogDto>().ReverseMap();
        }
    }
}