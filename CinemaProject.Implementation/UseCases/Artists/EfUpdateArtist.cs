﻿using AutoMapper;
using CinemaProject.Application.UseCases.Artists;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Artists
{
    public class EfUpdateArtist : EfGenericUpdate<ArtistDto, Artist>, IUpdateArtist
    {
        public EfUpdateArtist(CinemaProjectContext context, IMapper mapper, UpdateArtistValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateArtist";
    }
}