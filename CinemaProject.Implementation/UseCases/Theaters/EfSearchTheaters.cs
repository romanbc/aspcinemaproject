﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Theaters;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Theaters
{
    public class EfSearchTheaters : EfGenericSearch<Theater, TheaterDto, TheaterSearch>, ISearchTheaters
    {
        private readonly CinemaProjectContext _context;

        public EfSearchTheaters(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchTheaters";

        public override PagedResponse<TheaterDto> Execute(TheaterSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x => x.Name.ToLower().Contains(search.Name.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Cinema),
                    PerformFilter = x =>
                        x.Where(x =>
                            x.Cinema.Name.ToLower().Contains(search.Cinema.ToLower())
                            || x.Cinema.Address.ToLower().Contains(search.Cinema.ToLower())
                            || x.Cinema.City.Name.ToLower().Contains(search.Cinema.ToLower())
                            || x.Cinema.City.Country.Name.ToLower().Contains(search.Cinema.ToLower())
                        )
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<Theater>> Query => () => _context.Theaters.Include(x => x.Cinema).ThenInclude(x => x.City).ThenInclude(x => x.Country).Include(x => x.Seats);
    }
}