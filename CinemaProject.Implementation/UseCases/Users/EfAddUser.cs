﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Users;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Users
{
    public class EfAddUser : IAddUser
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;
        private readonly AddUserValidator _validator;
        private readonly IApplicationActor _actor;

        public EfAddUser(CinemaProjectContext context, IMapper mapper, AddUserValidator validator, IApplicationActor actor)
        {
            _context = context;
            _mapper = mapper;
            _validator = validator;
            _actor = actor;
        }

        public void Execute(UserDto request)
        {
            request.Id = 0;
            
            if (!_actor.IsSuperAdmin)
            {
                _validator.Include(new CheckForSuperAdminPrivilegesValidator(_context));
            }

            _validator.ValidateAndThrow(request);

            _context.Users.Add(_mapper.Map<UserDto, User>(request));
            _context.SaveChanges();
        }

        public string Id => "AddUser";
    }
}