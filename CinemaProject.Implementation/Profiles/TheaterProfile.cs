﻿using AutoMapper;
using CinemaProject.Application.UseCases.Theaters;
using CinemaProject.Domain.Entities;

namespace CinemaProject.Implementation.Profiles
{
    public class TheaterProfile : Profile
    {
        public TheaterProfile()
        {
            CreateMap<Theater, TheaterDto>()
                .ForMember(dto => dto.Cinema, o => o.MapFrom(t =>
                    t.Cinema.Name + ", " + t.Cinema.City.Name + ", " + t.Cinema.City.Country.Name)
                )
                .ForMember(dto => dto.Seats, o => o.MapFrom(t => t.Seats));

            CreateMap<TheaterDto, Theater>()
                .ForMember(c => c.Cinema, o => o.Ignore())
                .ForMember(c => c.Seats, o => o.Ignore());
        }
    }
}