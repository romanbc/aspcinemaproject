﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Reservations;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;

        public ReservationsController(UseCaseExecutor executor, IApplicationActor actor)
        {
            _executor = executor;
        }

        [HttpGet]
        public IActionResult Get([FromQuery] ReservationSearch search, [FromServices] ISearchReservations query)
        {            
            return Ok(_executor.ExecuteQuery(query, search));
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromServices] IFindReservation query)
        {
            return Ok(_executor.ExecuteQuery(query, id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] ReservationDto dto, [FromServices] IAddReservation command)
        {
            _executor.ExecuteCommand(command, dto);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ReservationDto dto, [FromServices] IUpdateReservation command)
        {
            dto.Id = id;
            _executor.ExecuteCommand(command, dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromServices] IDeleteReservation command)
        {
            _executor.ExecuteCommand(command, id);
            return NoContent();
        }
    }
}
