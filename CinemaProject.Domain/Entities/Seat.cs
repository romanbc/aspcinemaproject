﻿namespace CinemaProject.Domain.Entities
{
    public class Seat : BaseEntity
    {
        public int Row { get; set; }
        public int Number { get; set; }
        public int TheaterId { get; set; }

        public virtual Theater Theater { get; set; }
    }
}