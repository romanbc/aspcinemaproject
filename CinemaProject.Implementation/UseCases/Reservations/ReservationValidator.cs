﻿using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class ReservationValidator : AbstractValidator<ReservationDto>
    {
        public ReservationValidator(CinemaProjectContext context)
        {
            RuleFor(x => x.ScreeningId)
                .NotEmpty()
                .GreaterThan(0)
                .Must(x => context.Screenings.Any(c => c.Id == x))
                .WithMessage("Screening with that id does not exist")
                .Must((y, x) =>
                    context.Screenings.Find(x).ScreeningStartsAt >=
                    DateTime.UtcNow.AddMinutes(y.IsPaid ? 5 : 30)
                )
                .WithMessage("Tickets must be reserved/bought at least 30/5 minutes before screening starts");

            RuleFor(x => x)
                .Must(x =>
                    !x.IsReserved
                    || (x.CustomerId != null && x.CustomerId > 0)
                    || !string.IsNullOrWhiteSpace(x.Email)
                    || !string.IsNullOrWhiteSpace(x.PhoneNumber)
                )
                .WithMessage("When tickets are getting reserved and not bought in person at the cinema, some kind of contact information must be left");

            RuleFor(x => x)
                .Must(x => x.IsPaid || x.IsReserved)
                .WithMessage("Ticket must be reserved or bought, or both");

            RuleFor(x => x.CustomerId)
                .NotEmpty()
                .GreaterThan(0)
                .Must(x => context.Users.Any(c => c.Id == x))
                .WithMessage("Customer with that id does not exist")
                .When(x => x.CustomerId != null);

            RuleFor(x => x.FirstName)
                .NameValidation()
                .When(x => !string.IsNullOrWhiteSpace(x.FirstName));

            RuleFor(x => x.LastName)
                .NameValidation()
                .When(x => !string.IsNullOrWhiteSpace(x.LastName));

            RuleFor(x => x.Email)
                .EmailAddress()
                .MaximumLength(100)
                .When(x => !string.IsNullOrWhiteSpace(x.Email));

            RuleFor(x => x.PhoneNumber)
               .PhoneNumberValidation()
               .When(x => !string.IsNullOrWhiteSpace(x.PhoneNumber));

            RuleFor(x => x.SeatIds)
                .NotEmpty()
                .Must(x => x.Any() && x.Count() <= 10)
                .WithMessage("At least one seat must be choosen and maximum number of seats allowed is 10")
                .Must(x => x.Count() == x.Distinct().Count())
                .WithMessage("Seats must be unique");

            RuleForEach(x => x.SeatIds)
                .Must((y, x) => context.Seats.Any(s => s.Id == x && context.Screenings.Any(sc => sc.Id == y.ScreeningId && sc.TheaterId == s.TheaterId)))
                .WithMessage("Seat with an id of {PropertyValue} does not exist in the theater where screening is taking place")
                .Must((y, x) => !context.ReservedSeats.Any(rs => rs.SeatId == x && rs.ScreeningId == y.ScreeningId && rs.ReservationId != y.Id))
                .WithMessage("Seat with an id of {PropertyValue} is already taken");
        }
    }
}