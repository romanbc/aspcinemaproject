﻿using AutoMapper;
using CinemaProject.Application.UseCases.Countries;
using CinemaProject.Domain.Entities;
using System.Linq;

namespace CinemaProject.Implementation.Profiles
{
    public class CountryProfile : Profile
    {
        public CountryProfile()
        {
            CreateMap<Country, CountryDto>()
                 .ForMember(dto => dto.Cities, o => o.MapFrom(c => c.Cities.Select(x => x.Name)));

            CreateMap<CountryDto, Country>()
                 .ForMember(c => c.Cities, o => o.Ignore());
        }
    }
}