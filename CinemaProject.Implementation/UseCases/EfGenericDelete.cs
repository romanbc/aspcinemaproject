﻿using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;

namespace CinemaProject.Implementation.UseCases
{
    public abstract class EfGenericDelete<TEntity> : ICommand<int>
        where TEntity : BaseEntity
    {
        private readonly CinemaProjectContext _context;

        protected EfGenericDelete(CinemaProjectContext context)
        {
            _context = context;
        }

        public void Execute(int id)
        {
            var item = _context.Set<TEntity>().Find(id);

            if (item == null)
            {
                throw new EntityNotFoundException(id, typeof(TEntity));
            }

            if (CheckForConflicts(item))
            {
                throw new ConflictException(id);
            }

            item.DeletedAt = DateTime.UtcNow;
            RemoveChildren(item);
            _context.SaveChanges();
        }

        public abstract string Id { get; }

        protected virtual Func<TEntity, bool> CheckForConflicts => T => false;

        protected virtual Action<TEntity> RemoveChildren => T => { };
    }
}