﻿using AutoMapper;
using CinemaProject.Application.UseCases.Seats;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Seats
{
    public class EfUpdateSeat : EfGenericUpdate<SeatDto, Seat>, IUpdateSeat
    {
        public EfUpdateSeat(CinemaProjectContext context, IMapper mapper, UpdateSeatValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateSeat";
    }
}