﻿using CinemaProject.Application.UseCases.Seats;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Application.UseCases.Theaters
{
    public class TheaterDto : BaseDto
    {
        public string Name { get; set; }
        public int CinemaId { get; set; }
        public string Cinema { get; set; }
        public IEnumerable<SeatDto> Seats { get; set; }
        public int NumberOfSeats => Seats.Count();
    }
}