﻿namespace CinemaProject.Application.UseCases.Users
{
    public interface ISearchUsers : IQuery<UserSearch, PagedResponse<ReadUserDto>>
    {
    }
}