﻿using CinemaProject.Application;
using CinemaProject.Application.UseCases.Email;
using Microsoft.AspNetCore.Mvc;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;

        public EmailController(UseCaseExecutor executor)
        {
            _executor = executor;
        }

        [HttpPost]
        public IActionResult Post([FromBody] SendEmailDto dto, [FromServices] ISendEmail command)
        {
            _executor.ExecuteCommand(command, dto);
            return Ok();
        }
    }
}