﻿namespace CinemaProject.Application.UseCases.Users
{
    public interface IDeleteUser : ICommand<int>
    {
    }
}