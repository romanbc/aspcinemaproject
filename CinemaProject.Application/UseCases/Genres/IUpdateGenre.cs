﻿namespace CinemaProject.Application.UseCases.Genres
{
    public interface IUpdateGenre : ICommand<GenreDto>
    {
    }
}