﻿namespace CinemaProject.Application.UseCases.Screenings
{
    public interface IDeleteScreening : ICommand<int>
    {
    }
}