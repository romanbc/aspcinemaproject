﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Application.UseCases.UseCaseLogs
{
    public interface ISearchUseCaseLogs : IQuery<UseCaseLogSearch, PagedResponse<UseCaseLogDto>>
    {
    }
}
