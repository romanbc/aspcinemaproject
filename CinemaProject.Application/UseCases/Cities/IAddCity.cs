﻿namespace CinemaProject.Application.UseCases.Cities
{
    public interface IAddCity : ICommand<CityDto>
    {
    }
}