﻿using CinemaProject.Application.UseCases.Movies;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Movies
{
    public class AddMovieValidator : AbstractValidator<MovieDto>
    {
        public AddMovieValidator(CinemaProjectContext context)
        {
            Include(new MovieValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Movies.Any(m => m.Title == x.Title && m.ReleaseDate == x.ReleaseDate))
                .WithMessage("Movie with the same title and release date already exists");
        }
    }
}