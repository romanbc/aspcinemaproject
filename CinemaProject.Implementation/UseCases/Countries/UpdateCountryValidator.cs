﻿using CinemaProject.Application.UseCases.Countries;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Countries
{
    public class UpdateCountryValidator : AbstractValidator<CountryDto>
    {
        public UpdateCountryValidator(CinemaProjectContext context)
        {
            Include(new CountryValidator(context, true));
        }
    }
}