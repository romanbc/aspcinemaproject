﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfFindReservation : IFindReservation
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;
        private readonly IApplicationActor _actor;

        public EfFindReservation(CinemaProjectContext context, IMapper mapper, IApplicationActor actor)
        {
            _context = context;
            _mapper = mapper;
            _actor = actor;
        }

        public string Id => "FindReservation";

        public string ChildOf => "SearchReservations";

        public ReservationDto Execute(int id)
        {
            var item = _context.Reservations.Include(x => x.ReservedSeats).ThenInclude(x => x.Seat).FirstOrDefault(x => x.Id == id && x.CustomerId == _actor.Id);

            if (item == null)
            {
                throw new EntityNotFoundException(id, typeof(Reservation));
            }

            return _mapper.Map<ReservationDto>(item);
        }
    }
}