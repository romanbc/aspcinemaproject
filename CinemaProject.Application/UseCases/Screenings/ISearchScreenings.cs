﻿namespace CinemaProject.Application.UseCases.Screenings
{
    public interface ISearchScreenings : IQuery<ScreeningSearch, PagedResponse<ReadScreeningDto>>
    {
    }
}