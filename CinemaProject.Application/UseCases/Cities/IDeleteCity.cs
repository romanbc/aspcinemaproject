﻿namespace CinemaProject.Application.UseCases.Cities
{
    public interface IDeleteCity : ICommand<int>
    {
    }
}