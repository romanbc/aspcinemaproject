﻿using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class UpdateReservationValidator : AbstractValidator<ReservationDto>
    {
        public UpdateReservationValidator(CinemaProjectContext context)
        {
            Include(new ReservationValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Reservations.Any(r =>
                        r.ScreeningId == x.ScreeningId
                        && r.IsReserved
                        && (
                            (r.CustomerId != null && r.CustomerId == x.CustomerId)
                            || (r.Email != null && r.Email == x.Email)
                            || (r.PhoneNumber != null && r.PhoneNumber == x.PhoneNumber)
                        )
                        && r.Id != x.Id
                    )
                )
                .WithMessage("Customer has already reserved/bought tickets for that screening");
        }
    }
}