﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Countries;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Countries
{
    public class EfSearchCountries : EfGenericSearch<Country, CountryDto, CountrySearch>, ISearchCountries
    {
        private readonly CinemaProjectContext _context;

        public EfSearchCountries(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchCountries";

        public override PagedResponse<CountryDto> Execute(CountrySearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x => x.Name.ToLower().Contains(search.Name.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Iso3Code),
                    PerformFilter = x => x.Where(x => x.Iso3Code.ToLower().Contains(search.Iso3Code.ToLower()))
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<Country>> Query => () => _context.Countries.Include(x => x.Cities);
    }
}