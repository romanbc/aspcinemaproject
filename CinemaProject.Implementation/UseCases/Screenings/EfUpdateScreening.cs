﻿using AutoMapper;
using CinemaProject.Application.UseCases.Screenings;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Screenings
{
    public class EfUpdateScreening : EfGenericUpdate<ScreeningDto, Screening>, IUpdateScreening
    {
        public EfUpdateScreening(CinemaProjectContext context, IMapper mapper, UpdateScreeningValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateScreening";
    }
}