﻿using AutoMapper;
using CinemaProject.Application.UseCases.Roles;
using CinemaProject.Domain.Entities;
using CinemaProject.Domain.Relations;
using System.Linq;

namespace CinemaProject.Implementation.Profiles
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<Role, RoleDto>()
                .ForMember(dto => dto.UseCases, o => o.MapFrom(r => r.RoleUseCases.Select(ruc => ruc.UseCaseId)));

            CreateMap<RoleDto, Role>()
                .ForMember(r => r.RoleUseCases, o => o.MapFrom(dto => dto.UseCases.Select(uc =>
                    new RoleUseCase { UseCaseId = uc })));
        }
    }
}