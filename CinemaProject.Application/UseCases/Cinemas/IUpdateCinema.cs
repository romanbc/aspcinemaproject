﻿namespace CinemaProject.Application.UseCases.Cinemas
{
    public interface IUpdateCinema : ICommand<CinemaDto>
    {
    }
}