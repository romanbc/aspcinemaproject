﻿using AutoMapper;
using CinemaProject.Application.UseCases.Genres;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Genres
{
    public class EfFindGenre : EfGenericFind<Genre, GenreDto>, IFindGenre
    {
        private readonly CinemaProjectContext _context;

        public EfFindGenre(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindGenre";

        public override string ChildOf => "SearchGenres";

        protected override Func<IQueryable<Genre>> Query => () =>
              _context.Genres.Include(x => x.GenreMovies).ThenInclude(x => x.Movie);
    }
}