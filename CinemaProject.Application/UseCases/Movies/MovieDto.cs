﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CinemaProject.Application.UseCases.Movies
{
    public class MovieDto : BaseDto
    {
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Description { get; set; }
        public int Runtime { get; set; }
        public DateTime AvailableFrom { get; set; }
        public DateTime AvailableUntill { get; set; }
        public IFormFile ImageFile { get; set; }
        public IEnumerable<int> GenreIds { get; set; }
        public IEnumerable<MovieCreditDto> Credits { get; set; }
        public string CreditsAsJson { get; set; }
    }

    public class ReadMovieDto : BaseDto
    {
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Description { get; set; }
        public int Runtime { get; set; }
        public DateTime AvailableFrom { get; set; }
        public DateTime AvailableUntill { get; set; }
        public string Image { get; set; }
        public IEnumerable<string> Genres { get; set; }
        public IEnumerable<ReadMovieCreditDto> Credits { get; set; }
        public IEnumerable<ReadMovieScreeningDto> Screenings { get; set; }
    }

    public class ReadMovieCreditDto
    {
        public string Artist { get; set; }
        public string ArtistType { get; set; }
    }

    public class MovieCreditDto
    {
        public int ArtistId { get; set; }
        public int ArtistTypeId { get; set; }
    }

    public class ReadMovieScreeningDto
    {
        public int Id { get; set; }
        public DateTime ScreeningStartsAt { get; set; }
        public decimal Price { get; set; }
    }

    public class MovieCreditDtoEqualityComparer : IEqualityComparer<MovieCreditDto>
    {
        public bool Equals(MovieCreditDto b1, MovieCreditDto b2)
        {
            if (b2 == null && b1 == null)
                return true;
            else if (b1 == null || b2 == null)
                return false;
            else if (b1.ArtistId == b2.ArtistId && b1.ArtistTypeId == b2.ArtistTypeId)
                return true;
            else
                return false;
        }

        public int GetHashCode([DisallowNull] MovieCreditDto obj)
        {
            return (obj.ArtistId ^ obj.ArtistTypeId).GetHashCode();
        }
    }
}