﻿using AutoMapper;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.Domain.Relations;
using System.Linq;

namespace CinemaProject.Implementation.Profiles
{
    public class ReservationProfile : Profile
    {
        public ReservationProfile()
        {
            CreateMap<Reservation, ReservationDto>()
                .ForMember(dto => dto.ReservedSeats, o => o.MapFrom(r => r.ReservedSeats.Select(rs => rs.Seat)));

            CreateMap<ReservationDto, Reservation>()
                .ForMember(r => r.ReservedSeats, o => o.MapFrom(dto => dto.SeatIds.Select(s =>
                   new ReservedSeat
                   {
                       SeatId = s,
                       ScreeningId = dto.ScreeningId
                   }
               )));
        }
    }
}