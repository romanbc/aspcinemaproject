﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfAddReservation : IAddReservation
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;
        private readonly AddReservationValidator _validator;
        private readonly IApplicationActor _actor;
        private readonly IPaymentProcessor _paymentProcessor;

        public EfAddReservation(CinemaProjectContext context, IMapper mapper, AddReservationValidator validator, IApplicationActor actor, IPaymentProcessor paymentProcessor)
        {
            _context = context;
            _mapper = mapper;
            _validator = validator;
            _actor = actor;
            _paymentProcessor = paymentProcessor;
        }

        public void Execute(ReservationDto request)
        {
            request.CustomerId = _actor.Id;

            if (request.IsPaid)
            {
                if (!_paymentProcessor.AreTicketsPaid((int)request.CustomerId, request.ScreeningId, DateTime.UtcNow))
                {
                    throw new UnauthorizedUseCaseException(this, _actor);
                }
            }

            request.Id = 0;

            var user = _context.Users.Find(request.CustomerId);

            request.FirstName = user.FirstName;
            request.LastName = user.LastName;
            request.PhoneNumber = user.PhoneNumber;
            request.Email = user.Email;

            _validator.ValidateAndThrow(request);
            _context.Reservations.Add(_mapper.Map<ReservationDto, Reservation>(request));
            _context.SaveChanges();
        }

        public string Id => "AddReservation";
    }
}