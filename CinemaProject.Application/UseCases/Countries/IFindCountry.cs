﻿namespace CinemaProject.Application.UseCases.Countries
{
    public interface IFindCountry : IQuery<int, CountryDto>
    {
    }
}