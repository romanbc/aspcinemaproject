﻿namespace CinemaProject.Application.UseCases.Movies
{
    public interface IDeleteMovie : ICommand<int>
    {
    }
}