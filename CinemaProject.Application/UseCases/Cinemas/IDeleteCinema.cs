﻿namespace CinemaProject.Application.UseCases.Cinemas
{
    public interface IDeleteCinema : ICommand<int>
    {
    }
}