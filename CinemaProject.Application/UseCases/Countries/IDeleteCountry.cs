﻿namespace CinemaProject.Application.UseCases.Countries
{
    public interface IDeleteCountry : ICommand<int>
    {
    }
}