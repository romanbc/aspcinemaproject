﻿using CinemaProject.Application.UseCases.Genres;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Genres
{
    public class EfDeleteGenre : EfGenericDelete<Genre>, IDeleteGenre
    {
        public EfDeleteGenre(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteGenre";
    }
}