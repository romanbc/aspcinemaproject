﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cinemas
{
    public class EfSearchCinemas : EfGenericSearch<Cinema, CinemaDto, CinemaSearch>, ISearchCinemas
    {
        private readonly CinemaProjectContext _context;

        public EfSearchCinemas(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchCinemas";

        public override PagedResponse<CinemaDto> Execute(CinemaSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x => x.Name.ToLower().Contains(search.Name.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Location),
                    PerformFilter = x =>
                        x.Where(x =>
                            x.Address.ToLower().Contains(search.Location.ToLower())
                            || x.City.Name.ToLower().Contains(search.Location.ToLower())
                            || x.City.Country.Name.ToLower().Contains(search.Location.ToLower())
                        )
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<Cinema>> Query => () => _context.Cinemas.Include(x => x.City).ThenInclude(x => x.Country).Include(x => x.Theaters);
    }
}