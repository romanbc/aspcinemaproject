﻿using AutoMapper;
using CinemaProject.Application.UseCases.Cities;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cities
{
    public class EfFindCity : EfGenericFind<City, CityDto>, IFindCity
    {
        private readonly CinemaProjectContext _context;

        public EfFindCity(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindCity";

        public override string ChildOf => "SearchCities";

        protected override Func<IQueryable<City>> Query => () => _context.Cities.Include(x => x.Country);
    }
}