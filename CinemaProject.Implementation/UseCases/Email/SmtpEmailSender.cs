﻿using CinemaProject.Application.UseCases.Email;
using System.Net;
using System.Net.Mail;

namespace CinemaProject.Implementation.UseCases.Email
{
    public class SmtpEmailSender : IEmailSender
    {
        private readonly string _from;
        private readonly string _password;

        public SmtpEmailSender(string from, string password)
        {
            _from = from;
            _password = password;
        }

        public void Send(SendEmailDto mail)
        {
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_from, _password)
            };

            var message = new MailMessage(_from, mail.SendTo)
            {
                Subject = mail.Subject,
                Body = mail.Content,
                IsBodyHtml = true
            };

            smtp.Send(message);
        }
    }
}