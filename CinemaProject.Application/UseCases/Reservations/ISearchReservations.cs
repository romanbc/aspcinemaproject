﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface ISearchReservations : IQuery<ReservationSearch, PagedResponse<ReservationDto>>
    {
    }
}