﻿using AutoMapper;
using CinemaProject.Application.UseCases.Genres;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Genres
{
    public class EfUpdateGenre : EfGenericUpdate<GenreDto, Genre>, IUpdateGenre
    {
        public EfUpdateGenre(CinemaProjectContext context, IMapper mapper, UpdateGenreValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateGenre";
    }
}