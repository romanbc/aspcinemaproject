﻿using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cinemas
{
    public class EfDeleteCinema : EfGenericDelete<Cinema>, IDeleteCinema
    {
        public EfDeleteCinema(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteCinema";

        protected override Func<Cinema, bool> CheckForConflicts => x => x.Theaters.Any();
    }
}