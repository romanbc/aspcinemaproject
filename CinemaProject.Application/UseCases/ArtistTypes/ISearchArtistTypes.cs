﻿namespace CinemaProject.Application.UseCases.ArtistTypes
{
    public interface ISearchArtistTypes : IQuery<ArtistTypeSearch, PagedResponse<ArtistTypeDto>>
    {
    }
}