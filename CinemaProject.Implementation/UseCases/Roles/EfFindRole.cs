﻿using AutoMapper;
using CinemaProject.Application.UseCases.Roles;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Roles
{
    public class EfFindRole : EfGenericFind<Role, RoleDto>, IFindRole
    {
        private readonly CinemaProjectContext _context;

        public EfFindRole(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindRole";

        public override string ChildOf => "SearchRoles";

        protected override Func<IQueryable<Role>> Query => () => _context.Roles.Include(x => x.RoleUseCases);
    }
}