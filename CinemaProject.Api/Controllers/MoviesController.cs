﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Movies;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;
        public MoviesController(UseCaseExecutor executor)
        {
            _executor = executor;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get([FromQuery] MovieSearch search, [FromServices] ISearchMovies query)
        {
            return Ok(_executor.ExecuteQuery(query, search));
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromServices] IFindMovie query)
        {
            return Ok(_executor.ExecuteQuery(query, id));
        }

        [HttpPost]
        public IActionResult Post([FromForm] MovieDto dto, [FromServices] IAddMovie command)
        {
            dto.DeserializeMovieCredits();

            _executor.ExecuteCommand(command, dto);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromForm] MovieDto dto, [FromServices] IUpdateMovie command)
        {
            dto.Id = id;
            dto.DeserializeMovieCredits();

            _executor.ExecuteCommand(command, dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromServices] IDeleteMovie command)
        {
            _executor.ExecuteCommand(command, id);
            return NoContent();
        }
    }

    public static class HelpWithSendingArrayOfObjectWhenUsingFormDataInsteadOfJson
    {
        //tackles problem of sending array of custom objects when form data (FromForm)
        //is being used instead of json (FromBody)        

        //[{"artistId":1,"artistTypeId":1}, {"artistId":1,"artistTypeId":2}]

        public static void DeserializeMovieCredits(this MovieDto dto)
        {
            try
            {
                var Credits = JsonConvert.DeserializeObject<IEnumerable<MovieCreditDto>>(dto.CreditsAsJson);

                foreach (var credit in Credits)
                {
                    if (credit.ArtistId == 0 || credit.ArtistTypeId == 0)
                    {
                        throw new Exception();
                    }
                }

                dto.Credits = Credits;
            }
            catch (Exception)
            {
                dto.Credits = new List<MovieCreditDto>();
            }
        }
    }
}
