﻿using AutoMapper;
using CinemaProject.Application.UseCases.Artists;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Artists
{
    public class EfFindArtist : EfGenericFind<Artist, ArtistDto>, IFindArtist
    {
        private readonly CinemaProjectContext _context;

        public EfFindArtist(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindArtist";

        public override string ChildOf => "SearchArtists";

        protected override Func<IQueryable<Artist>> Query => () => _context.Artists.Include(x => x.PlaceOfBirth).ThenInclude(x => x.Country);
    }
}