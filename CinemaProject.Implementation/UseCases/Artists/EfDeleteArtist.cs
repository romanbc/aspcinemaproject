﻿using CinemaProject.Application.UseCases.Artists;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Artists
{
    public class EfDeleteArtist : EfGenericDelete<Artist>, IDeleteArtist
    {
        public EfDeleteArtist(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteArtist";
    }
}