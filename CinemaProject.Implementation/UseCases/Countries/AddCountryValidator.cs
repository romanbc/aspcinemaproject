﻿using CinemaProject.Application.UseCases.Countries;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Countries
{
    public class AddCountryValidator : AbstractValidator<CountryDto>
    {
        public AddCountryValidator(CinemaProjectContext context)
        {
            Include(new CountryValidator(context));
        }
    }
}