﻿using AutoMapper;
using CinemaProject.Application.UseCases.Countries;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Countries
{
    public class EfAddCountry : EfGenericAdd<CountryDto, Country>, IAddCountry
    {
        public EfAddCountry(CinemaProjectContext context, IMapper mapper, AddCountryValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddCountry";
    }
}