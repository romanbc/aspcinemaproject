﻿using AutoMapper;
using CinemaProject.Application.UseCases.Screenings;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Screenings
{
    public class EfFindScreening : EfGenericFind<Screening, ReadScreeningDto>, IFindScreening
    {
        private readonly CinemaProjectContext _context;

        public EfFindScreening(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindScreening";

        public override string ChildOf => "SearchScreenings";

        protected override Func<IQueryable<Screening>> Query => () => _context.Screenings
              .Include(x => x.Theater)
                  .ThenInclude(x => x.Cinema)
              .Include(x => x.Theater)
                  .ThenInclude(x => x.Seats)
              .Include(x => x.Movie)
              .Include(x => x.ReservedSeats)
                  .ThenInclude(x => x.Seat)
              .AsSingleQuery();
    }
}