﻿using CinemaProject.Domain.Entities;

namespace CinemaProject.Domain.Relations
{
    public class ReservedSeat
    {
        public int SeatId { get; set; }
        public int ReservationId { get; set; }
        public int ScreeningId { get; set; }

        public virtual Seat Seat { get; set; }
        public virtual Reservation Reservation { get; set; }
        public virtual Screening Screening { get; set; }
    }
}