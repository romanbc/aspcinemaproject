﻿using AutoMapper;
using CinemaProject.Application.UseCases.Genres;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Genres
{
    public class EfAddGenre : EfGenericAdd<GenreDto, Genre>, IAddGenre
    {
        public EfAddGenre(CinemaProjectContext context, IMapper mapper, AddGenreValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddGenre";
    }
}