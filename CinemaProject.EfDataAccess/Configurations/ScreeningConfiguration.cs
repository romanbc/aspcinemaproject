﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    public class ScreeningConfiguration : IEntityTypeConfiguration<Screening>
    {
        public void Configure(EntityTypeBuilder<Screening> builder)
        {
            //builder.HasIndex(s => new { s.TheaterId, s.ScreeningStartsAt }).IsUnique();

            builder.HasOne(s => s.Movie)
               .WithMany(m => m.Screenings)
               .HasForeignKey(s => s.MovieId)
               .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(s => s.Theater)
               .WithMany()
               .HasForeignKey(s => s.TheaterId)
               .OnDelete(DeleteBehavior.Restrict);
        }
    }
}