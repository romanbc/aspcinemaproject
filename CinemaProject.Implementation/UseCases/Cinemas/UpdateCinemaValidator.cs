﻿using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cinemas
{
    public class UpdateCinemaValidator : AbstractValidator<CinemaDto>
    {
        public UpdateCinemaValidator(CinemaProjectContext context)
        {
            Include(new CinemaValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Cinemas.Any(c =>
                        c.Name == x.Name
                        && c.CityId == x.CityId
                        && c.Id != x.Id
                    )
                )
                .WithMessage("Cinema with the same name already exists in that city");
        }
    }
}