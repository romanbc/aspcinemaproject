﻿using CinemaProject.Domain.Relations;
using System;
using System.Collections.Generic;

namespace CinemaProject.Domain.Entities
{
    public class Screening : BaseEntity
    {
        public int MovieId { get; set; }
        public int TheaterId { get; set; }
        public DateTime ScreeningStartsAt { get; set; }
        public decimal Price { get; set; }

        public virtual Movie Movie { get; set; }
        public virtual Theater Theater { get; set; }

        public virtual ICollection<Reservation> Reservations { get; set; } = new HashSet<Reservation>();
        public virtual ICollection<ReservedSeat> ReservedSeats { get; set; } = new HashSet<ReservedSeat>();
    }
}