﻿using AutoMapper;
using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.ArtistTypes
{
    public class EfAddArtistType : EfGenericAdd<ArtistTypeDto, ArtistType>, IAddArtistType
    {
        public EfAddArtistType(CinemaProjectContext context, IMapper mapper, AddArtistTypeValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddArtistType";
    }
}