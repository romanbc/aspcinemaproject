﻿namespace CinemaProject.Application.UseCases.Email
{
    public interface ISendEmail : ICommand<SendEmailDto>
    {
    }
}