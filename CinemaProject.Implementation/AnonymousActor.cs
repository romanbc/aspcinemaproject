﻿using CinemaProject.Application;
using System.Collections.Generic;

namespace CinemaProject.Implementation
{
    public class AnonymousActor : IApplicationActor
    {
        public int Id => 0;
        public bool IsSuperAdmin => false;
        public string Identity => "Anonymus";

        public IEnumerable<string> AllowedUseCases => new List<string>() { 
            "SearchArtists",
            "SearchArtistTypes",
            "SearchCinemas",
            "SearchCities",
            "SearchCountries",
            "SearchGenres",
            "SearchMovies",
            "SearchScreenings",
            "SearchSeats",
            "SearchTheaters"
        };
    }
}
