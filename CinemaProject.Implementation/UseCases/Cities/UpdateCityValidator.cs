﻿using CinemaProject.Application.UseCases.Cities;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cities
{
    public class UpdateCityValidator : AbstractValidator<CityDto>
    {
        public UpdateCityValidator(CinemaProjectContext context)
        {
            Include(new CityValidator(context));

            RuleFor(x => x.Name)
                .Must((y, x) => !context.Cities.Any(c => c.Name == x && c.CountryId == y.CountryId && c.Id != y.Id))
                .WithMessage("Country already has city with the same name.");
        }
    }
}