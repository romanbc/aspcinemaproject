﻿using AutoMapper;
using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cinemas
{
    public class EfFindCinema : EfGenericFind<Cinema, CinemaDto>, IFindCinema
    {
        private readonly CinemaProjectContext _context;

        public EfFindCinema(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindCinema";

        public override string ChildOf => "SearchCinemas";
        
        protected override Func<IQueryable<Cinema>> Query => () => _context.Cinemas.Include(x => x.City).ThenInclude(x => x.Country).Include(x => x.Theaters);
    }
}