﻿namespace CinemaProject.Application.UseCases.Genres
{
    public interface ISearchGenres : IQuery<GenreSearch, PagedResponse<GenreDto>>
    {
    }
}