﻿namespace CinemaProject.Application.UseCases.Roles
{
    public interface IDeleteRole : ICommand<int>
    {
    }
}