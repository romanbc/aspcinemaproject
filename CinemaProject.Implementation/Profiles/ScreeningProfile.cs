﻿using AutoMapper;
using CinemaProject.Application.UseCases.Screenings;
using CinemaProject.Domain.Entities;
using System;
using System.Linq;

namespace CinemaProject.Implementation.Profiles
{
    public class ScreeningProfile : Profile
    {
        public ScreeningProfile()
        {
            CreateMap<Screening, ReadScreeningDto>()
                .ForMember(dto => dto.Movie, o => o.MapFrom(s => s.Movie.Title))
                .ForMember(dto => dto.Theater, o => o.MapFrom(s => s.Theater.Name + ", " + s.Theater.Cinema.Name))
                .ForMember(dto => dto.MovieRuntime, o => o.MapFrom(s => s.Movie.Runtime))
                .ForMember(dto => dto.ReservedSeats, o => o.MapFrom(s => s.ReservedSeats.Select(rs => rs.Seat)))
                .ForMember(dto => dto.AvailableSeats, o => o.MapFrom(s =>
                    s.Theater.Seats.Where(seat =>
                        !s.ReservedSeats.Select(rs => rs.SeatId).Contains(seat.Id))
                ));

            CreateMap<ScreeningDto, Screening>();
        }
    }
}