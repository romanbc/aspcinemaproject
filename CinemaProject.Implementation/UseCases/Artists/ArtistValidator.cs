﻿using CinemaProject.Application.UseCases.Artists;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Artists
{
    public class ArtistValidator : AbstractValidator<ArtistDto>
    {
        public ArtistValidator(CinemaProjectContext context)
        {
            RuleFor(x => x.FirstName)
                .NameValidation();

            RuleFor(x => x.LastName)
                .NameValidation();

            RuleFor(x => x.Gender)
                .NotEmpty()
                .Must(x => Enum.IsDefined(typeof(Gender), x))
                .WithMessage("Gender does not exist");

            RuleFor(x => x.PlaceOfBirthId)
                .NotEmpty()
                .Must(x => context.Cities.Any(c => c.Id == x))
                .WithMessage("City with that id does not exist");

            RuleFor(x => x.DateOfBirth)
                .NotEmpty()
                .InclusiveBetween(new DateTime(1800, 1, 1), DateTime.UtcNow.AddYears(-1))
                .WithMessage("Artist could have been born in the last 200 years");
        }
    }
}