﻿namespace CinemaProject.Application.UseCases.Seats
{
    public interface IAddSeat : ICommand<SeatDto>
    {
    }
}