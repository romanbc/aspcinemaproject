﻿namespace CinemaProject.Application.UseCases.Cities
{
    public interface IFindCity : IQuery<int, CityDto>
    {
    }
}