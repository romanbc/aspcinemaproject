﻿namespace CinemaProject.Application.UseCases.Movies
{
    public interface IUpdateMovie : ICommand<MovieDto>
    {
    }
}