﻿using AutoMapper;
using CinemaProject.Application.UseCases.Genres;
using CinemaProject.Domain.Entities;
using System.Linq;

namespace CinemaProject.Implementation.Profiles
{
    public class GenreProfile : Profile
    {
        public GenreProfile()
        {
            CreateMap<Genre, GenreDto>()
                .ForMember(dto => dto.Movies, o => o.MapFrom(g => g.GenreMovies.Select(gm => gm.Movie.Title)));

            CreateMap<GenreDto, Genre>();
        }
    }
}