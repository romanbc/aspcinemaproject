﻿namespace CinemaProject.Application.UseCases.Users
{
    public interface IUpdateUser : ICommand<UserDto>
    {
    }
}