﻿namespace CinemaProject.Application.UseCases.Movies
{
    public interface ISearchMovies : IQuery<MovieSearch, PagedResponse<ReadMovieDto>>
    {
    }
}