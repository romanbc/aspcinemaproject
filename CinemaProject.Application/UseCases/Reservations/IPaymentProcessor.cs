﻿using System;

namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IPaymentProcessor
    {
        bool AreTicketsPaid(int customerId, int screeningId, DateTime timestamp);
    }
}