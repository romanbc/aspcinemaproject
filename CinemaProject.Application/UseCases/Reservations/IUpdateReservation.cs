﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IUpdateReservation : ICommand<ReservationDto>
    {
    }
}