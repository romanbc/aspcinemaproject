﻿namespace CinemaProject.Application.UseCases.Screenings
{
    public interface IUpdateScreening : ICommand<ScreeningDto>
    {
    }
}