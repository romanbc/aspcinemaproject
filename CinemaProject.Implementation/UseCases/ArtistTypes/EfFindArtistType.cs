﻿using AutoMapper;
using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.ArtistTypes
{
    public class EfFindArtistType : EfGenericFind<ArtistType, ArtistTypeDto>, IFindArtistType
    {
        public EfFindArtistType(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public override string Id => "FindArtistType";

        public override string ChildOf => "SearchArtistTypes";
    }
}