﻿using CinemaProject.Application;
using CinemaProject.Implementation;
using CinemaProject.Implementation.Exceptions;
using FluentAssertions;
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using Xunit;

namespace CinemaProject.Tests
{
    public class UseCaseTests
    {
        [Fact]
        public void DoesNotThrowException_WhenUseCasePropertiesAreValid()
        {
            Action a = () => UseCaseConfig.AreUseCasesValid();
            a.Should().NotThrow();
        }

        [Theory]
        [InlineData("FindCountry", "SearchCountries")]
        [InlineData("AddMoVIe")]
        [InlineData("DeleteGenre ", "UpdateGenre")]
        public void ThrowsException_WhenUseCaseIdsAreNotUnique(string id, string childOf = null)
        {
            Type[] types = PrepareTypes(id, childOf);

            Action a = () => UseCaseConfig.AreUseCasesValid(types);
            a.Should().ThrowExactly<UseCaseIdNotUniqueException>();
        }

        [Theory]
        [InlineData("AddObject", "DoSomething")]
        [InlineData("MakeSomething", "AddRoless")]
        public void ThrowsException_WhenUseCaseChildOfUsesNonExistentId(string id, string childOf)
        {
            Type[] types = PrepareTypes(id, childOf);

            Action a = () => UseCaseConfig.AreUseCasesValid(types);
            a.Should().ThrowExactly<UseCaseChildOfNotFoundException>();
        }

        public static Type[] PrepareTypes(string id, string childOf)
        {
            Type testType = TypeBuilderHelper.CreateTypeDerivedFromUseCase(id, childOf);

            var types = typeof(UseCaseConfig).Assembly.GetTypes().ToList();
            types.Add(testType);

            return types.ToArray();
        }
    }

    public static class TypeBuilderHelper
    {
        public static Type CreateTypeDerivedFromUseCase(string id, string childOf)
        {
            AssemblyName assemblyName = new("DynamicTestAssembly");
            AssemblyBuilder assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("DynamicTestModule");
            TypeBuilder typeBuilder = moduleBuilder.DefineType("TestInvalidUseCase", TypeAttributes.Public);

            typeBuilder.AddInterfaceImplementation(typeof(IUseCase));

            #region Constructors

            Type[] parameterTypes = { typeof(object) };
            ConstructorBuilder ctor1 = typeBuilder.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard,
                parameterTypes);

            ILGenerator ctor1IL = ctor1.GetILGenerator();

            ctor1IL.Emit(OpCodes.Ldarg_0);
            ctor1IL.Emit(OpCodes.Call,
                typeof(object).GetConstructor(Type.EmptyTypes));
            ctor1IL.Emit(OpCodes.Ret);

            #endregion Constructors

            typeBuilder.CreateOverrideForStringProperty("Id", id);

            if (childOf != null)
            {
                typeBuilder.CreateOverrideForStringProperty("ChildOf", childOf);
            }

            return typeBuilder.CreateType();
        }

        private static void CreateOverrideForStringProperty(this TypeBuilder typeBuilder, string propertyName, string value)
        {
            PropertyBuilder propertyBuilder = typeBuilder.DefineProperty(propertyName, PropertyAttributes.HasDefault, typeof(string), null);

            MethodBuilder getPropertyMethodBuilder = typeBuilder.DefineMethod("get_" + propertyName, MethodAttributes.Virtual | MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig, typeof(string), Type.EmptyTypes);

            ILGenerator getIL = getPropertyMethodBuilder.GetILGenerator();

            getIL.Emit(OpCodes.Ldstr, value);
            getIL.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getPropertyMethodBuilder);
        }
    }
}