﻿using AutoMapper;
using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.ArtistTypes
{
    public class EfUpdateArtistType : EfGenericUpdate<ArtistTypeDto, ArtistType>, IUpdateArtistType
    {
        public EfUpdateArtistType(CinemaProjectContext context, IMapper mapper, UpdateArtistTypeValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateArtistType";
    }
}