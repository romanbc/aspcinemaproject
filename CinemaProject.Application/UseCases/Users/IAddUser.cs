﻿namespace CinemaProject.Application.UseCases.Users
{
    public interface IAddUser : ICommand<UserDto>
    {
    }
}