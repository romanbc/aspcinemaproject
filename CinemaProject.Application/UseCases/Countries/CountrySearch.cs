﻿namespace CinemaProject.Application.UseCases.Countries
{
    public class CountrySearch : PagedSearch
    {
        public string Iso3Code { get; set; }
        public string Name { get; set; }
    }
}