﻿using AutoMapper;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfFindReservationAsEmployee : EfGenericFind<Reservation, ReservationDto>, IFindReservationAsEmployee
    {
        private readonly CinemaProjectContext _context;

        public EfFindReservationAsEmployee(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindEmployeeMadeReservation";

        public override string ChildOf => "SearchEmployeeMadeReservations";

        protected override Func<IQueryable<Reservation>> Query => () => _context.Reservations.Include(x => x.ReservedSeats).ThenInclude(x => x.Seat);
    }
}