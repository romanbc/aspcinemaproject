﻿using AutoMapper;
using CinemaProject.Application.UseCases.Seats;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Seats
{
    public class EfAddSeat : EfGenericAdd<SeatDto, Seat>, IAddSeat
    {
        public EfAddSeat(CinemaProjectContext context, IMapper mapper, AddSeatValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddSeat";
    }
}