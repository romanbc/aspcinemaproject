﻿using System;

namespace CinemaProject.Implementation.Exceptions
{
    public class UseCaseChildOfNotFoundException : Exception
    {
        public UseCaseChildOfNotFoundException(string message) : base(message)
        {
        }
    }
}