﻿using AutoMapper;
using CinemaProject.Application.UseCases.Users;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Users
{
    public class EfFindUser : EfGenericFind<User, ReadUserDto>, IFindUser
    {
        private readonly CinemaProjectContext _context;

        public EfFindUser(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindUser";

        public override string ChildOf => "SearchUsers";

        protected override Func<IQueryable<User>> Query => () => _context.Users.Include(x => x.UserRoles).ThenInclude(x => x.Role).ThenInclude(x => x.RoleUseCases).Include(x => x.UserUseCases);
    }
}