﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IFindReservation : IQuery<int, ReservationDto>
    {
    }
}