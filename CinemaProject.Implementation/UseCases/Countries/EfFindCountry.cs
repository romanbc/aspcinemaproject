﻿using AutoMapper;
using CinemaProject.Application.UseCases.Countries;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Countries
{
    public class EfFindCountry : EfGenericFind<Country, CountryDto>, IFindCountry
    {
        private readonly CinemaProjectContext _context;

        public EfFindCountry(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindCountry";

        public override string ChildOf => "SearchCountries";

        protected override Func<IQueryable<Country>> Query => () => _context.Countries.Include(x => x.Cities);
    }
}