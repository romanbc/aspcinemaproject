﻿using CinemaProject.Application.UseCases.Genres;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Genres
{
    public class AddGenreValidator : AbstractValidator<GenreDto>
    {
        public AddGenreValidator(CinemaProjectContext context)
        {
            Include(new GenreValidator(context));
        }
    }
}