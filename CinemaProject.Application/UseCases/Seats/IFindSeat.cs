﻿namespace CinemaProject.Application.UseCases.Seats
{
    public interface IFindSeat : IQuery<int, SeatDto>
    {
    }
}