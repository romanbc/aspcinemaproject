﻿using CinemaProject.Application.UseCases.Roles;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Roles
{
    public class RoleValidator : AbstractValidator<RoleDto>
    {
        public RoleValidator(CinemaProjectContext context, bool isUpdate = false)
        {
            RuleFor(x => x.Name)
              .NameValidation()
              .CheckIfAlreadyExists<RoleDto, Role>(context, "Name", isUpdate);

            RuleFor(x => x.UseCases)
                .Must(x => x.Count() == x.Distinct().Count())
                .WithMessage("Use cases must be unique")
                .When(x => x.UseCases != null && x.UseCases.Any());

            RuleForEach(x => x.UseCases)
                .Must(x => UseCaseConfig.UseCaseDocNames.Contains(x))
                .WithMessage("Use case '{PropertyValue}' does not exist");
        }
    }
}