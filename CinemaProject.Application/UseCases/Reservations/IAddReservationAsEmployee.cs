﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IAddReservationAsEmployee : ICommand<ReservationDto>
    {
    }
}