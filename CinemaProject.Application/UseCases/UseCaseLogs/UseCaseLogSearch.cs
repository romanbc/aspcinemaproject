﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Application.UseCases.UseCaseLogs
{
    public class UseCaseLogSearch : PagedSearch
    {
        public string UseCaseName { get; set; }        
        public string Actor { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
