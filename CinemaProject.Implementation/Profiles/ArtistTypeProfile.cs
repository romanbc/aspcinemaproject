﻿using AutoMapper;
using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.Domain.Entities;

namespace CinemaProject.Implementation.Profiles
{
    public class ArtistTypeProfile : Profile
    {
        public ArtistTypeProfile()
        {
            CreateMap<ArtistType, ArtistTypeDto>().ReverseMap();
        }
    }
}