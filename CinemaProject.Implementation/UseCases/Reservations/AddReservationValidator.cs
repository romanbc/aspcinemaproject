﻿using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class AddReservationValidator : AbstractValidator<ReservationDto>
    {
        public AddReservationValidator(CinemaProjectContext context)
        {
            Include(new ReservationValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Reservations.Any(r =>
                        r.ScreeningId == x.ScreeningId
                        && r.IsReserved
                        && (
                            (r.CustomerId != null && r.CustomerId == x.CustomerId)
                            || (r.Email != null && r.Email == x.Email)
                            || (r.PhoneNumber != null && r.PhoneNumber == x.PhoneNumber)
                        )
                    )
                )
                .WithMessage("Customer has already reserved/bought tickets for that screening");
        }
    }
}