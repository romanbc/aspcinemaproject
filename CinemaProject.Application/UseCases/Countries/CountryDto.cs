﻿using System.Collections.Generic;

namespace CinemaProject.Application.UseCases.Countries
{
    public class CountryDto : BaseDto
    {
        public string Iso3Code { get; set; }
        public string Name { get; set; }
        public IEnumerable<string> Cities { get; set; }
    }
}