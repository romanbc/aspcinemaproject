﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    internal class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            //builder.HasIndex(m => new { m.Title, m.ReleaseDate }).IsUnique();

            builder.Property(m => m.Title).IsRequired().HasMaxLength(100);
        }
    }
}