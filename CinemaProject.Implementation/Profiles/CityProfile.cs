﻿using AutoMapper;
using CinemaProject.Application.UseCases.Cities;
using CinemaProject.Domain.Entities;

namespace CinemaProject.Implementation.Profiles
{
    public class CityProfile : Profile
    {
        public CityProfile()
        {
            CreateMap<City, CityDto>()
                .ForMember(dto => dto.Country, o => o.MapFrom(c => c.Country.Iso3Code + " - " + c.Country.Name));

            CreateMap<CityDto, City>()
                .ForMember(c => c.Country, o => o.Ignore());
        }
    }
}