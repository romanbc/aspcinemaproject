﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Seats;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Seats
{
    public class EfSearchSeats : EfGenericSearch<Seat, SeatDto, SeatSearch>, ISearchSeats
    {
        public EfSearchSeats(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public override string Id => "SearchSeats";

        public override PagedResponse<SeatDto> Execute(SeatSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => search.Number.HasValue,
                    PerformFilter = x => x.Where(x => x.Number == search.Number)
                },
                new Filter
                {
                    IsConditionMet = () => search.Row.HasValue,
                    PerformFilter = x => x.Where(x => x.Row == search.Row)
                },
                new Filter
                {
                    IsConditionMet = () => search.TheaterId.HasValue,
                    PerformFilter = x => x.Where(x => x.TheaterId == search.TheaterId)
                }
            };

            return Execute(search, filters);
        }
    }
}