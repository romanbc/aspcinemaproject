﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Cinemas;
using Microsoft.AspNetCore.Authorization;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CinemasController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;
        public CinemasController(UseCaseExecutor executor)
        {
            _executor = executor;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get([FromQuery] CinemaSearch search, [FromServices] ISearchCinemas query)
        {
            return Ok(_executor.ExecuteQuery(query, search));
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromServices] IFindCinema query)
        {
            return Ok(_executor.ExecuteQuery(query, id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] CinemaDto dto, [FromServices] IAddCinema command)
        {
            _executor.ExecuteCommand(command, dto);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] CinemaDto dto, [FromServices] IUpdateCinema command)
        {
            dto.Id = id;
            _executor.ExecuteCommand(command, dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromServices] IDeleteCinema command)
        {
            _executor.ExecuteCommand(command, id);
            return NoContent();
        }
    }
}
