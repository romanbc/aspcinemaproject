﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public class ReservationSearch : PagedSearch
    {
        public int? ScreeningId { get; set; }
        public int? CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool? IsReserved { get; set; }
        public bool? IsPaid { get; set; }
        public int? SeatId { get; set; }
    }
}