﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    internal class ArtistConfiguration : IEntityTypeConfiguration<Artist>
    {
        public void Configure(EntityTypeBuilder<Artist> builder)
        {
            //builder.HasIndex(a => new { a.FirstName, a.LastName, a.DateOfBirth, a.PlaceOfBirthId }).IsUnique();

            builder.Property(a => a.FirstName).IsRequired().HasMaxLength(100);

            builder.Property(a => a.LastName).IsRequired().HasMaxLength(100);

            builder.HasOne(a => a.PlaceOfBirth)
               .WithMany()
               .HasForeignKey(a => a.PlaceOfBirthId)
               .OnDelete(DeleteBehavior.Restrict);
        }
    }
}