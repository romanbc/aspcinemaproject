﻿using CinemaProject.Application;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Api.Core
{
    public static class ContainerExtensions
    {
        public static void AddUseCases(this IServiceCollection services)
        {            
            services.RegisterImplementationsOf(typeof(IQuery<,>), typeof(AnonymousActor).Assembly);
            services.RegisterImplementationsOf(typeof(ICommand<>), typeof(AnonymousActor).Assembly);
            services.AddValidators();
            services.AddTransient<UseCaseExecutor>();
        }

        public static void RegisterImplementationsOf(this IServiceCollection services, Type parentType, System.Reflection.Assembly assembly, string implementationPrefix = "Ef")
        {
            ImplementationParams parameters = new(parentType, assembly, implementationPrefix);

            services.AddImplentationToInterface(parameters);
        }

        public static void AddImplentationToInterface(this IServiceCollection services,
           ImplementationParams parameters)
        {
            var interfaces = parameters.WhichInterface.Assembly.GetTypes()
                .Where(type =>
                    type.IsInterface &&
                    type.GetInterfaces()
                    .Any(@interface =>
                        @interface.Name == parameters.WhichInterface.Name
                    )
                )
                .ToList();

            var implementations = parameters.ImplementationAssembly.GetTypes()
               .Where(type =>
                   !type.IsAbstract &&
                   type.GetInterfaces()
                   .Any(@interface =>
                       @interface.GetInterfaces()
                       .Any(i =>
                           i.Name == parameters.WhichInterface.Name
                       )
                   )
               )
               .ToList();

            foreach (var @interface in interfaces)
            {
                Console.WriteLine(@interface.FullName);

                var implementation = implementations.FirstOrDefault(type =>
                        type.GetInterface(@interface.Name) != null &&
                        type.Name.StartsWith(parameters.ImplementationPrefix)
                    );

                if (implementation != null)
                {
                    Console.WriteLine("ima implementaciju");
                    Console.WriteLine(implementation.FullName);

                    services.AddTransient(@interface, implementation);
                }
                else
                {
                    Console.WriteLine("nema implementaciju");
                }
                Console.WriteLine();
            }
        }

        public static void AddValidators(this IServiceCollection services)
        {
            var validators = typeof(AnonymousActor).Assembly.GetTypes()
               .Where(type =>
                   !type.IsAbstract &&
                   type.BaseType.Name == typeof(AbstractValidator<>).Name
               )
               .ToList();

            foreach (var validator in validators)
            {
                Console.WriteLine(validator.FullName);

                var dto = validator.BaseType.GetGenericArguments().FirstOrDefault();

                Console.WriteLine(dto.FullName);

                var abstractValidatorForSpecificDto = typeof(AbstractValidator<>).MakeGenericType(dto);

                services.AddTransient(validator);
                services.AddTransient(abstractValidatorForSpecificDto, validator);

                Console.WriteLine();
            }
        }

        public static void AddApplicationActor(this IServiceCollection services)
        {
            services.AddTransient<IApplicationActor>(x =>
            {
                var accessor = x.GetService<IHttpContextAccessor>();

                var user = accessor.HttpContext.User;

                if (user.FindFirst("ActorData") == null)
                {
                    return new AnonymousActor();
                }

                var actorString = user.FindFirst("ActorData").Value;

                var actor = JsonConvert.DeserializeObject<JwtActor>(actorString);

                return actor;

            });
        }

        public static void AddJwt(this IServiceCollection services, AppSettings appSettings)
        {
            services.AddTransient<JwtManager>(x =>
            {
                var context = x.GetService<CinemaProjectContext>();

                return new JwtManager(context, appSettings.JwtIssuer, appSettings.JwtSecretKey);
            });

            services.AddAuthentication(options =>
            {
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(cfg =>
            {
                cfg.RequireHttpsMetadata = false;
                cfg.SaveToken = true;
                cfg.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = appSettings.JwtIssuer,
                    ValidateIssuer = true,
                    ValidAudience = "Any",
                    ValidateAudience = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.JwtSecretKey)),
                    ValidateIssuerSigningKey = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
            });
        }
    }

    public class ImplementationParams
    {
        public Type WhichInterface { get; }
        public System.Reflection.Assembly ImplementationAssembly { get; }
        public string ImplementationPrefix { get; }

        public ImplementationParams(Type whichInterface, System.Reflection.Assembly implementationAssembly,
                string implementationPrefix)
        {
            WhichInterface = whichInterface;
            ImplementationAssembly = implementationAssembly;
            ImplementationPrefix = implementationPrefix;
        }
    }    
}
