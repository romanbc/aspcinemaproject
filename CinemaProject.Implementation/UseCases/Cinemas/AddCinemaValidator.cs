﻿using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cinemas
{
    public class AddCinemaValidator : AbstractValidator<CinemaDto>
    {
        public AddCinemaValidator(CinemaProjectContext context)
        {
            Include(new CinemaValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Cinemas.Any(c =>
                        c.Name == x.Name
                        && c.CityId == x.CityId
                    )
                )
                .WithMessage("Cinema with the same name already exists in that city");
        }
    }
}