﻿namespace CinemaProject.Application.UseCases.ArtistTypes
{
    public class ArtistTypeDto : BaseDto
    {
        public string Name { get; set; }
    }
}