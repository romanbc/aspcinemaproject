﻿using AutoMapper;
using CinemaProject.Application.UseCases.Screenings;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Screenings
{
    public class EfAddScreening : EfGenericAdd<ScreeningDto, Screening>, IAddScreening
    {
        public EfAddScreening(CinemaProjectContext context, IMapper mapper, AddScreeningValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddScreening";
    }
}