﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    internal class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            //builder.HasIndex(c => new { c.CountryId, c.Name }).IsUnique();

            builder.Property(c => c.Name).IsRequired().HasMaxLength(100);

            builder.HasOne(c => c.Country)
               .WithMany(c => c.Cities)
               .HasForeignKey(c => c.CountryId)
               .OnDelete(DeleteBehavior.Restrict);
        }
    }
}