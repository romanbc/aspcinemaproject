using CinemaProject.Api.Core;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Email;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation;
using CinemaProject.Implementation.Logging;
using CinemaProject.Implementation.UseCases.Email;
using CinemaProject.Implementation.UseCases.Reservations;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;

namespace CinemaProject.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            CheckIfUseCasesAreValid();

            var appSettings = new AppSettings();
            Configuration.Bind(appSettings);

            services.AddTransient<CinemaProjectContext>();
            services.AddTransient<IUseCaseLogger, EfDatabaseUseCaseLogger>();
            services.AddTransient<IPaymentProcessor, FakePaymentProcessor>();
            services.AddTransient<IEmailSender, SmtpEmailSender>(x => 
                new SmtpEmailSender(appSettings.EmailFrom, appSettings.EmailPassword)
            );
            services.AddHttpContextAccessor();
            services.AddApplicationActor();
            services.AddUseCases();

#pragma warning disable CS0618 // Type or member is obsolete
            ValidatorOptions.Global.CascadeMode = CascadeMode.StopOnFirstFailure;
#pragma warning restore CS0618 // Type or member is obsolete

            services.AddJwt(appSettings);
            services.AddAutoMapper(typeof(AnonymousActor).Assembly);

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CinemaProject.Api", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = @"JWT Authorization header using the Bearer scheme. Enter 'Bearer' [space] and then your token in the text input below. Example: 'Bearer 12345abcdef'",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header
                            },
                            new List<string>()
                        }
                    });
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CinemaProject.Api v1"));
            }

            app.UseRouting();
            app.UseStaticFiles();
            app.UseMiddleware<GlobalExceptionHandler>();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireAuthorization();
            });
        }

        public static void CheckIfUseCasesAreValid()
        {
            try
            {
                UseCaseConfig.AreUseCasesValid();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Environment.Exit(1);
            }
        }
    }
}
