﻿namespace CinemaProject.Application.UseCases.Cities
{
    public class CityDto : BaseDto
    {
        public string Name { get; set; }
        public int CountryId { get; set; }
        public string Country { get; set; }
    }
}