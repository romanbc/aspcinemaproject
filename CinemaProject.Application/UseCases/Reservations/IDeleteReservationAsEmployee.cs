﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IDeleteReservationAsEmployee : ICommand<int>
    {
    }
}