﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Users;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Users
{
    public class EfSearchUsers : EfGenericSearch<User, ReadUserDto, UserSearch>, ISearchUsers
    {
        private readonly CinemaProjectContext _context;

        public EfSearchUsers(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchUsers";

        public override PagedResponse<ReadUserDto> Execute(UserSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.FirstName),
                    PerformFilter = x => x.Where(x => x.FirstName.ToLower().Contains(search.FirstName.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.LastName),
                    PerformFilter = x => x.Where(x => x.LastName.ToLower().Contains(search.LastName.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x =>
                        x.FirstName.ToLower().Contains(search.Name.ToLower())
                        || x.LastName.ToLower().Contains(search.Name.ToLower())
                    )
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Email),
                    PerformFilter = x => x.Where(x => x.Email.ToLower().Contains(search.Email.ToLower()))
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<User>> Query => () => _context.Users.Include(x => x.UserRoles).ThenInclude(x => x.Role).ThenInclude(x => x.RoleUseCases).Include(x => x.UserUseCases);
    }
}