﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IFindReservationAsEmployee : IQuery<int, ReservationDto>
    {
    }
}