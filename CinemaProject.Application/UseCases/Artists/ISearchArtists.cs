﻿namespace CinemaProject.Application.UseCases.Artists
{
    public interface ISearchArtists : IQuery<ArtistSearch, PagedResponse<ArtistDto>>
    {
    }
}
