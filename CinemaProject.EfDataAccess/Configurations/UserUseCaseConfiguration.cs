﻿using CinemaProject.Domain.Relations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    public class UserUseCaseConfiguration : IEntityTypeConfiguration<UserUseCase>
    {
        public void Configure(EntityTypeBuilder<UserUseCase> builder)
        {
            builder.HasKey(uuc => new { uuc.UserId, uuc.UseCaseId });

            builder.Property(uuc => uuc.UseCaseId).IsRequired().HasMaxLength(100);
        }
    }
}