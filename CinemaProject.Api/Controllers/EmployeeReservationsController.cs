﻿using CinemaProject.Application;
using CinemaProject.Application.UseCases.Reservations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeReservationsController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;

        public EmployeeReservationsController(UseCaseExecutor executor)
        {
            _executor = executor;
        }

        [HttpGet]
        public IActionResult Get([FromQuery] ReservationSearch search, [FromServices] ISearchReservationsAsEmployee query)
        {
            return Ok(_executor.ExecuteQuery(query, search));
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromServices] IFindReservationAsEmployee query)
        {
            return Ok(_executor.ExecuteQuery(query, id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] ReservationDto dto, [FromServices] IAddReservationAsEmployee command)
        {
            _executor.ExecuteCommand(command, dto);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ReservationDto dto, [FromServices] IUpdateReservationAsEmployee command)
        {
            dto.Id = id;
            _executor.ExecuteCommand(command, dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromServices] IDeleteReservationAsEmployee command)
        {
            _executor.ExecuteCommand(command, id);
            return NoContent();
        }
    }
}
