﻿using CinemaProject.Application.UseCases.Theaters;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Theaters
{
    public class TheaterValidator : AbstractValidator<TheaterDto>
    {
        public TheaterValidator(CinemaProjectContext context)
        {
            RuleFor(x => x.Name)
                 .TitleValidation();

            RuleFor(x => x.CinemaId)
                .NotEmpty()
                .Must(x => context.Cinemas.Any(c => c.Id == x))
                .WithMessage("Cinema with an id of {PropertyValue} does not exist");
        }
    }
}