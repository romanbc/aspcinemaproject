﻿namespace CinemaProject.Application.UseCases.Cinemas
{
    public interface IFindCinema : IQuery<int, CinemaDto>
    {
    }
}