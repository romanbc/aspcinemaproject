﻿using CinemaProject.Application;
using CinemaProject.Application.UseCases.Countries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;
        public CountriesController(UseCaseExecutor executor)
        {
            _executor = executor;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get([FromQuery] CountrySearch search, [FromServices] ISearchCountries query)
        {
            return Ok(_executor.ExecuteQuery(query, search));
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromServices] IFindCountry query)
        {
            return Ok(_executor.ExecuteQuery(query, id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] CountryDto dto, [FromServices] IAddCountry command)
        {
            _executor.ExecuteCommand(command, dto);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] CountryDto dto, [FromServices] IUpdateCountry command)
        {
            dto.Id = id;
            _executor.ExecuteCommand(command, dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromServices] IDeleteCountry command)
        {
            _executor.ExecuteCommand(command, id);
            return NoContent();
        }
    }
}
