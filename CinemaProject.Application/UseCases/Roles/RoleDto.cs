﻿using System.Collections.Generic;

namespace CinemaProject.Application.UseCases.Roles
{
    public class RoleDto : BaseDto
    {
        public string Name { get; set; }
        public IEnumerable<string> UseCases { get; set; }
    }
}