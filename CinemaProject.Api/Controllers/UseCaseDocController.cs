﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UseCaseDocController : ControllerBase
    {
        [HttpGet]
        [Route("getUseCaseNames")]
        public IEnumerable<object> GetUseCaseNames()
        {
            return Implementation.UseCaseConfig.UseCaseDocNames;
        }
    }
}
