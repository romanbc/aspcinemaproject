﻿using CinemaProject.EfDataAccess;
using CinemaProject.Implementation;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Api.Core
{
    public class JwtManager
    {
        private readonly CinemaProjectContext _context;
        private readonly string _issuer;
        private readonly string _secretKey;

        public JwtManager(CinemaProjectContext context, string issuer, string secretKey)
        {
            _context = context;
            _issuer = issuer;
            _secretKey = secretKey;
        }

        public string MakeToken(string email, string password)
        {
            var user = 
                _context.Users
                    .Include(u => u.UserUseCases)
                    .Include(u => u.UserRoles)
                        .ThenInclude(ur => ur.Role)
                            .ThenInclude(r => r.RoleUseCases)
                .FirstOrDefault(x => x.Email == email);

            if (user == null)
            {
                return null;
            }

            var verified = BCrypt.Net.BCrypt.Verify(password, user.Password);
            if (!verified)
            {
                return null;
            }

            var actor = new JwtActor
            {
                Id = user.Id,
                IsSuperAdmin = user.UserRoles.Any(x => x.Role.Name == "SuperAdmin"),

                AllowedUseCases = 
                    user.UserRoles.SelectMany(ur =>
                            ur.Role.RoleUseCases.Select(ruc =>
                                ruc.UseCaseId
                            )
                    ).Where(x => !user.UserUseCases.Any(uuc => uuc.UseCaseId == x && !uuc.IsAllowed)).Union(user.UserUseCases.Where(uuc => uuc.IsAllowed).Select(uuc => uuc.UseCaseId)),

                Identity = user.Email
                

            };

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString(), ClaimValueTypes.String, _issuer),
                new Claim(JwtRegisteredClaimNames.Iss, _issuer, ClaimValueTypes.String, _issuer),
                new Claim(JwtRegisteredClaimNames.Iat, DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString(), ClaimValueTypes.Integer64, _issuer),
                new Claim("UserId", actor.Id.ToString(), ClaimValueTypes.String, _issuer),
                new Claim("ActorData", JsonConvert.SerializeObject(actor), ClaimValueTypes.String, _issuer)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_secretKey));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var now = DateTime.UtcNow;
            var token = new JwtSecurityToken(
                issuer: _issuer,
                audience: "Any",
                claims: claims,
                notBefore: now,
                expires: now.AddMinutes(1200),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
