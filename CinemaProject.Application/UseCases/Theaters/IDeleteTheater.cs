﻿namespace CinemaProject.Application.UseCases.Theaters
{
    public interface IDeleteTheater : ICommand<int>
    {
    }
}