﻿using CinemaProject.Domain.Relations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    internal class CreditConfiguration : IEntityTypeConfiguration<Credit>
    {
        public void Configure(EntityTypeBuilder<Credit> builder)
        {
            builder.HasKey(x => new { x.MovieId, x.ArtistId, x.ArtistTypeId });

            builder.HasOne(c => c.Artist)
                .WithMany(a => a.Credits)
                .HasForeignKey(c => c.ArtistId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(c => c.ArtistType)
                .WithMany(at => at.Credits)
                .HasForeignKey(c => c.ArtistTypeId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(c => c.Movie)
                .WithMany(m => m.Credits)
                .HasForeignKey(c => c.MovieId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}