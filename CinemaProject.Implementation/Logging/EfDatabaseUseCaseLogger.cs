﻿using CinemaProject.Application;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Newtonsoft.Json;
using System;

namespace CinemaProject.Implementation.Logging
{
    public class EfDatabaseUseCaseLogger : IUseCaseLogger
    {
        private readonly CinemaProjectContext _context;

        public EfDatabaseUseCaseLogger(CinemaProjectContext context)
        {
            _context = context;
        }

        public void Log(IUseCase useCase, IApplicationActor actor, object useCaseData)
        {
            _context.UseCaseLogs.Add(new UseCaseLog
            {
                Actor = actor.Identity,
                Data = JsonConvert.SerializeObject(useCaseData),
                Date = DateTime.UtcNow,
                UseCaseName = useCase.Name
            });

            _context.SaveChanges();
        }
    }
}