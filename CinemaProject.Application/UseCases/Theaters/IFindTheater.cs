﻿namespace CinemaProject.Application.UseCases.Theaters
{
    public interface IFindTheater : IQuery<int, TheaterDto>
    {
    }
}