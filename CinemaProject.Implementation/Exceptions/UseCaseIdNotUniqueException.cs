﻿using System;

namespace CinemaProject.Implementation.Exceptions
{
    public class UseCaseIdNotUniqueException : Exception
    {
        public UseCaseIdNotUniqueException(string message) : base(message)
        {
        }
    }
}