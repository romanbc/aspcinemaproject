﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IDeleteReservation : ICommand<int>
    {
    }
}