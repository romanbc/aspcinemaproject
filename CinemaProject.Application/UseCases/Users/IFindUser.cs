﻿namespace CinemaProject.Application.UseCases.Users
{
    public interface IFindUser : IQuery<int, ReadUserDto>
    {
    }
}