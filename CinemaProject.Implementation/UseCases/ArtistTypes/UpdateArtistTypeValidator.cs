﻿using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.ArtistTypes
{
    public class UpdateArtistTypeValidator : AbstractValidator<ArtistTypeDto>
    {
        public UpdateArtistTypeValidator(CinemaProjectContext context)
        {
            Include(new ArtistTypeValidator(context, true));
        }
    }
}