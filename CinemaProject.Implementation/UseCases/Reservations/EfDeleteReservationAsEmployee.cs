﻿using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfDeleteReservationAsEmployee : EfGenericDelete<Reservation>, IDeleteReservationAsEmployee
    {
        public EfDeleteReservationAsEmployee(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteEmployeeMadeReservation";

        protected override Action<Reservation> RemoveChildren => x => x.ReservedSeats.Clear();
    }
}