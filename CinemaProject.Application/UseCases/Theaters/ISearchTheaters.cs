﻿namespace CinemaProject.Application.UseCases.Theaters
{
    public interface ISearchTheaters : IQuery<TheaterSearch, PagedResponse<TheaterDto>>
    {
    }
}