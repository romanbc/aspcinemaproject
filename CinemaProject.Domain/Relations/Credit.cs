﻿using CinemaProject.Domain.Entities;

namespace CinemaProject.Domain.Relations
{
    public class Credit
    {
        public int MovieId { get; set; }
        public int ArtistId { get; set; }
        public int ArtistTypeId { get; set; }

        public virtual ArtistType ArtistType { get; set; }
        public virtual Movie Movie { get; set; }
        public virtual Artist Artist { get; set; }
    }
}