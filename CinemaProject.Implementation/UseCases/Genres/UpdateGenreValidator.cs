﻿using CinemaProject.Application.UseCases.Genres;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Genres
{
    public class UpdateGenreValidator : AbstractValidator<GenreDto>
    {
        public UpdateGenreValidator(CinemaProjectContext context)
        {
            Include(new GenreValidator(context, true));
        }
    }
}