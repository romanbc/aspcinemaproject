﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Screenings;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Screenings
{
    public class EfSearchScreenings : EfGenericSearch<Screening, ReadScreeningDto, ScreeningSearch>, ISearchScreenings
    {
        private readonly CinemaProjectContext _context;

        public EfSearchScreenings(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchScreenings";

        public override PagedResponse<ReadScreeningDto> Execute(ScreeningSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Movie),
                    PerformFilter = x => x.Where(x => x.Movie.Title.ToLower().Contains(search.Movie.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Cinema),
                    PerformFilter = x => x.Where(x => x.Theater.Cinema.Name.ToLower().Contains(search.Cinema.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Theater),
                    PerformFilter = x => x.Where(x => x.Theater.Name.ToLower().Contains(search.Theater.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => search.MovieId.HasValue,
                    PerformFilter = x => x.Where(x => x.MovieId == search.MovieId)
                },
                new Filter
                {
                    IsConditionMet = () => search.TheaterId.HasValue,
                    PerformFilter = x => x.Where(x => x.TheaterId == search.TheaterId)
                },
                new Filter
                {
                    IsConditionMet = () => search.CinemaId.HasValue,
                    PerformFilter = x => x.Where(x => x.Theater.CinemaId == search.CinemaId)
                },
                new Filter
                {
                    IsConditionMet = () => search.ScreeningStartsAtFrom.HasValue,
                    PerformFilter = x => x.Where(x => x.ScreeningStartsAt >= search.ScreeningStartsAtFrom)
                },
                new Filter
                {
                    IsConditionMet = () => search.ScreeningStartsAtTo.HasValue,
                    PerformFilter = x => x.Where(x => x.ScreeningStartsAt <= search.ScreeningStartsAtTo)
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<Screening>> Query => () => _context.Screenings
              .Include(x => x.Theater)
                  .ThenInclude(x => x.Cinema)
              .Include(x => x.Theater)
                  .ThenInclude(x => x.Seats)
              .Include(x => x.Movie)
              .Include(x => x.ReservedSeats)
                  .ThenInclude(x => x.Seat)
              .AsSingleQuery();
    }
}