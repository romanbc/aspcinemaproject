﻿using CinemaProject.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CinemaProject.Api.Core
{
    public class JwtActor : IApplicationActor
    {
        public int Id { get; set; }
        public string Identity { get; set; }
        public bool IsSuperAdmin { get; set; }
        
        public IEnumerable<string> AllowedUseCases { get; set; }
    }
}
