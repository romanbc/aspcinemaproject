﻿namespace CinemaProject.Application.UseCases.Countries
{
    public interface IUpdateCountry : ICommand<CountryDto>
    {
    }
}