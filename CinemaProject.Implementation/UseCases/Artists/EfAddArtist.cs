﻿using AutoMapper;
using CinemaProject.Application.UseCases.Artists;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Artists
{
    public class EfAddArtist : EfGenericAdd<ArtistDto, Artist>, IAddArtist
    {
        public EfAddArtist(CinemaProjectContext context, IMapper mapper, AddArtistValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddArtist";
    }
}