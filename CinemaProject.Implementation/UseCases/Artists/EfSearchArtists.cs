﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Artists;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Artists
{
    public class EfSearchArtists : EfGenericSearch<Artist, ArtistDto, ArtistSearch>, ISearchArtists
    {
        private readonly CinemaProjectContext _context;

        public EfSearchArtists(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchArtists";

        public override PagedResponse<ArtistDto> Execute(ArtistSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.FirstName),
                    PerformFilter = x => x.Where(x => x.FirstName.ToLower().Contains(search.FirstName.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.LastName),
                    PerformFilter = x => x.Where(x => x.LastName.ToLower().Contains(search.LastName.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x =>
                        x.FirstName.ToLower().Contains(search.Name.ToLower())
                        || x.LastName.ToLower().Contains(search.Name.ToLower())
                    )
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.PlaceOfBirth),
                    PerformFilter = x => x.Where(x =>
                        x.PlaceOfBirth.Name.ToLower().Contains(search.PlaceOfBirth.ToLower())
                        || x.PlaceOfBirth.Country.Name.ToLower().Contains(search.PlaceOfBirth.ToLower())
                    )
                },
                new Filter
                {
                    IsConditionMet = () => search.DateOfBirthFrom.HasValue,
                    PerformFilter = x => x.Where(x => x.DateOfBirth >= search.DateOfBirthFrom)
                },
                new Filter
                {
                    IsConditionMet = () => search.DateOfBirthTo.HasValue,
                    PerformFilter = x => x.Where(x => x.DateOfBirth <= search.DateOfBirthTo)
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<Artist>> Query => () => _context.Artists.Include(x => x.PlaceOfBirth).ThenInclude(x => x.Country);
    }
}