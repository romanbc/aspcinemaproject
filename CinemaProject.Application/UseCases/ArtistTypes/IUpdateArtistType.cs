﻿namespace CinemaProject.Application.UseCases.ArtistTypes
{
    public interface IUpdateArtistType : ICommand<ArtistTypeDto>
    {
    }
}