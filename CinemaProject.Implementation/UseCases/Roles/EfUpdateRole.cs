﻿using AutoMapper;
using CinemaProject.Application.UseCases.Roles;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Roles
{
    public class EfUpdateRole : EfGenericUpdate<RoleDto, Role>, IUpdateRole
    {
        public EfUpdateRole(CinemaProjectContext context, IMapper mapper, UpdateRoleValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateRole";
    }
}