﻿using System;

namespace CinemaProject.Application.UseCases.Movies
{
    public class MovieSearch : PagedSearch
    {
        public string Title { get; set; }
        public DateTime? ReleaseDateFrom { get; set; }
        public DateTime? ReleaseDateTo { get; set; }
        public string Genre { get; set; }
        public string Artist { get; set; }
    }
}