﻿using CinemaProject.Application.UseCases.Countries;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Countries
{
    internal class CountryValidator : AbstractValidator<CountryDto>
    {
        public CountryValidator(CinemaProjectContext context, bool isUpdate = false)
        {
            RuleFor(x => x.Name)
                .NameValidation()
                .CheckIfAlreadyExists<CountryDto, Country>(context, "Name", isUpdate);

            RuleFor(x => x.Iso3Code)
                .NotEmpty()
                .Matches(@"^[\p{Lu}]{3}$")
                .WithMessage("Iso3Code consists of 3 uppercase letters")
                .CheckIfAlreadyExists<CountryDto, Country>(context, "Iso3Code", isUpdate);
        }
    }
}