﻿namespace CinemaProject.Application.UseCases.Artists
{
    public interface IUpdateArtist : ICommand<ArtistDto>
    {
    }
}
