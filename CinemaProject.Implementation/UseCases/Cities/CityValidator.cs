﻿using CinemaProject.Application.UseCases.Cities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cities
{
    public class CityValidator : AbstractValidator<CityDto>
    {
        public CityValidator(CinemaProjectContext context)
        {
            RuleFor(x => x.Name)
                .NameValidation();

            RuleFor(x => x.CountryId)
                .NotEmpty()
                .GreaterThan(0)
                .Must(x => context.Countries.Any(c => c.Id == x))
                .WithMessage("Country with that id does not exist");
        }
    }
}