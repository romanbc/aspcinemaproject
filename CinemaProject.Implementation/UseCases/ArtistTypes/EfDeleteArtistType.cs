﻿using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.ArtistTypes
{
    public class EfDeleteArtistType : EfGenericDelete<ArtistType>, IDeleteArtistType
    {
        public EfDeleteArtistType(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteArtistType";
    }
}