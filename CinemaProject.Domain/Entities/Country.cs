﻿using System.Collections.Generic;

namespace CinemaProject.Domain.Entities
{
    public class Country : BaseEntity
    {
        public string Iso3Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<City> Cities { get; set; } = new HashSet<City>();
    }
}