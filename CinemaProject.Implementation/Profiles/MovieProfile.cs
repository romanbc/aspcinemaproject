﻿using AutoMapper;
using CinemaProject.Application.UseCases.Movies;
using CinemaProject.Domain.Entities;
using CinemaProject.Domain.Relations;
using CinemaProject.Implementation.Extensions;
using System.Linq;

namespace CinemaProject.Implementation.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, ReadMovieDto>()
                .ForMember(dto => dto.Genres, o => o.MapFrom(m => m.MovieGenres.Select(mg => mg.Genre.Name)))
                .ForMember(dto => dto.Credits, o => o.MapFrom(m => m.Credits.Select(c =>
                    new ReadMovieCreditDto
                    {
                        Artist = c.Artist.FirstName + " " + c.Artist.LastName,
                        ArtistType = c.ArtistType.Name
                    }
                )))
                .ForMember(dto => dto.Screenings, o => o.MapFrom(m => m.Screenings.Select(s =>
                    new ReadMovieScreeningDto
                    {
                        Id = s.Id,
                        Price = s.Price,
                        ScreeningStartsAt = s.ScreeningStartsAt
                    }
                )));

            CreateMap<MovieDto, Movie>()
                .ForMember(m => m.Image, o =>
                    {
                        o.PreCondition(dto => dto.ImageFile != null);
                        o.MapFrom(dto => dto.ImageFile.UploadFile("images/movies"));
                    }
                )
                .ForMember(m => m.MovieGenres, o => o.MapFrom(dto => dto.GenreIds.Select(g => new MovieGenre
                {
                    GenreId = g
                })))
                .ForMember(m => m.Credits, o => o.MapFrom(dto => dto.Credits.Select(c => new Credit
                {
                    ArtistId = c.ArtistId,
                    ArtistTypeId = c.ArtistTypeId
                })));
        }
    }
}