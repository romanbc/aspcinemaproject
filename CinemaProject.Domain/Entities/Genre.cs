﻿using CinemaProject.Domain.Relations;
using System.Collections.Generic;

namespace CinemaProject.Domain.Entities
{
    public class Genre : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<MovieGenre> GenreMovies { get; set; }
    }
}