﻿namespace CinemaProject.Application.UseCases.ArtistTypes
{
    public interface IFindArtistType : IQuery<int, ArtistTypeDto>
    {
    }
}