﻿using CinemaProject.Application.UseCases.Theaters;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Theaters
{
    public class EfDeleteTheater : EfGenericDelete<Theater>, IDeleteTheater
    {
        private readonly CinemaProjectContext _context;

        public EfDeleteTheater(CinemaProjectContext context) : base(context)
        {
            _context = context;
        }

        public override string Id => "DeleteTheater";

        protected override Func<Theater, bool> CheckForConflicts => x => _context.Screenings.Any(s => s.TheaterId == x.Id);
    }
}