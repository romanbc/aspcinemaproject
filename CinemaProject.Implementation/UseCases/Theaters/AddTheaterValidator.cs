﻿using CinemaProject.Application.UseCases.Theaters;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Theaters
{
    public class AddTheaterValidator : AbstractValidator<TheaterDto>
    {
        public AddTheaterValidator(CinemaProjectContext context)
        {
            Include(new TheaterValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Theaters.Any(c =>
                        c.Name == x.Name
                        && c.CinemaId == x.CinemaId
                    )
                )
                .WithMessage("Theater with the same name is already present in this cinema");
        }
    }
}