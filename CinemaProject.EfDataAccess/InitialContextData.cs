﻿using CinemaProject.Domain.Entities;
using CinemaProject.Domain.Relations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.EfDataAccess
{
    internal static class InitialContextData
    {
        internal static List<Role> Roles => new()
        {
            new Role
            {
                Id = 1,
                CreatedAt = DateTime.Now,
                Name = "SuperAdmin"
            },
            new Role
            {
                Id = 2,
                CreatedAt = DateTime.Now,
                Name = "Admin"
            },
            new Role
            {
                Id = 3,
                CreatedAt = DateTime.Now,
                Name = "Program Director"
            },
            new Role
            {
                Id = 4,
                CreatedAt = DateTime.Now,
                Name = "Cinema Manager"
            },
            new Role
            {
                Id = 5,
                CreatedAt = DateTime.Now,
                Name = "Box office"
            },
            new Role
            {
                Id = 6,
                CreatedAt = DateTime.Now,
                Name = "Employee"
            },
            new Role
            {
                Id = 7,
                CreatedAt = DateTime.Now,
                Name = "Customer"
            }
        };

        internal static List<RoleUseCase> RoleUseCases
        {
            get
            {
                var discoverableResources = new List<string>() {
                    "Artist", "ArtistType", "Cinema", "City", "Country",
                    "Genre", "Movie", "Screening", "Seat", "Theater"
                };

                List<RoleUseCase> list = new();

                list.GenerateRoleUseCases(2, "User");
                list.GenerateRoleUseCases(2, "Cinema");
                list.GenerateRoleUseCases(2, "City");
                list.GenerateRoleUseCases(2, "Country");

                list.GenerateRoleUseCases(3, "Artist");
                list.GenerateRoleUseCases(3, "ArtistType");
                list.GenerateRoleUseCases(3, "Movie");
                list.GenerateRoleUseCases(3, "Genre");

                list.GenerateRoleUseCases(4, "Theater");
                list.GenerateRoleUseCases(4, "Seat");
                list.GenerateRoleUseCases(4, "Screening");

                list.GenerateRoleUseCases(5, "EmployeeMadeReservation");

                discoverableResources.ForEach(x => list.Add(GenerateSearchUseCase(6, x)));
                list.Add(
                    new()
                    { 
                        RoleId = 6,
                        UseCaseId = "SendEmail"
                    }
                );

                list.GenerateRoleUseCases(7, "Reservation");
                discoverableResources.ForEach(x => list.Add(GenerateSearchUseCase(7, x)));

                return list;
            }
        }

        internal static List<User> Users => new()
        {
            new User
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                Email = "john.doe@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "John",
                LastName = "Doe",
                PhoneNumber = "+1 60 1234 501"
            },
            new User
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                Email = "jane.doe@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Jane",
                LastName = "Doe",
                PhoneNumber = "+1 60 1234 502"
            },
            new User
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                Email = "mike.johns@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Mike",
                LastName = "Johns",
                PhoneNumber = "+1 60 1234 503"
            },
            new User
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                Email = "anne.johns@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Anne",
                LastName = "Johns",
                PhoneNumber = "+1 60 1234 504"
            },
            new User
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                Email = "peter.smith@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Peter",
                LastName = "Smith",
                PhoneNumber = "+1 60 1234 505"
            },
            new User
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                Email = "chris.smith@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Chris",
                LastName = "Smith",
                PhoneNumber = "+1 60 1234 506"
            },
            new User
            {
                Id = 7,
                CreatedAt = DateTime.UtcNow,
                Email = "lisa.brown@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Lisa",
                LastName = "Brown",
                PhoneNumber = "+1 60 1234 507"
            },
            new User
            {
                Id = 8,
                CreatedAt = DateTime.UtcNow,
                Email = "robert.wilson@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Robert",
                LastName = "Wilson",
                PhoneNumber = "+1 60 1234 508"
            },
            new User
            {
                Id = 9,
                CreatedAt = DateTime.UtcNow,
                Email = "robert.wilson@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Robert",
                LastName = "Wilson",
                PhoneNumber = "+1 60 1234 509"
            },
            new User
            {
                Id = 10,
                CreatedAt = DateTime.UtcNow,
                Email = "mary.davies@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Mary",
                LastName = "Davies",
                PhoneNumber = "+1 60 1234 510"
            },
            new User
            {
                Id = 11,
                CreatedAt = DateTime.UtcNow,
                Email = "emily.roberts@cinema.rs",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Emily",
                LastName = "Roberts",
                PhoneNumber = "+1 60 1234 511"
            },
            new User
            {
                Id = 12,
                CreatedAt = DateTime.UtcNow,
                Email = "sarah.wright.87@gmail.com",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Sarah",
                LastName = "Wright",
                PhoneNumber = "+1 60 9876 600"
            },
            new User
            {
                Id = 13,
                CreatedAt = DateTime.UtcNow,
                Email = "mark.thompson.02@gmail.com",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Mark",
                LastName = "Thompson",
                PhoneNumber = "+1 60 9176 600"
            },
            new User
            {
                Id = 14,
                CreatedAt = DateTime.UtcNow,
                Email = "daniel.walker@gmail.com",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Daniel",
                LastName = "Walker",
                PhoneNumber = "+1 60 9872 604"
            },
            new User
            {
                Id = 15,
                CreatedAt = DateTime.UtcNow,
                Email = "susan.jackson@gmail.com",
                Password = "$2a$11$NtGYYXXx4vzkdaXHQdqXj.jwsuTpnrqcUm2ZbZmKCVRfSJogxtTiu",
                FirstName = "Susan",
                LastName = "Jackson",
                PhoneNumber = "+1 60 9872 892"
            }
        };

        internal static List<UserRole> UserRoles => new()
        {
            new()
            {
                UserId = 1,
                RoleId = 1
            },
            new()
            {
                UserId = 2,
                RoleId = 2
            },
            new()
            {
                UserId = 2,
                RoleId = 6
            },
            new()
            {
                UserId = 3,
                RoleId = 2
            },
            new()
            {
                UserId = 3,
                RoleId = 6
            },
            new()
            {
                UserId = 4,
                RoleId = 3
            },
            new()
            {
                UserId = 4,
                RoleId = 6
            },
            new()
            {
                UserId = 5,
                RoleId = 3
            },
            new()
            {
                UserId = 5,
                RoleId = 4
            },
            new()
            {
                UserId = 5,
                RoleId = 6
            },
            new()
            {
                UserId = 6,
                RoleId = 4
            },
            new()
            {
                UserId = 6,
                RoleId = 6
            },
            new()
            {
                UserId = 7,
                RoleId = 5
            },
            new()
            {
                UserId = 7,
                RoleId = 6
            },
            new()
            {
                UserId = 8,
                RoleId = 5
            },
            new()
            {
                UserId = 8,
                RoleId = 6
            },
            new()
            {
                UserId = 9,
                RoleId = 5
            },
            new()
            {
                UserId = 9,
                RoleId = 6
            },
            new()
            {
                UserId = 10,
                RoleId = 5
            },
            new()
            {
                UserId = 10,
                RoleId = 6
            },
            new()
            {
                UserId = 11,
                RoleId = 5
            },
            new()
            {
                UserId = 11,
                RoleId = 6
            },
            new()
            {
                UserId = 12,
                RoleId = 7
            },
            new()
            {
                UserId = 13,
                RoleId = 7
            },
            new()
            {
                UserId = 14,
                RoleId = 7
            },
            new()
            {
                UserId = 15,
                RoleId = 7
            }
        };

        internal static List<UserUseCase> UserUseCases => new()
        {
            new()
            {
                UserId = 4,
                UseCaseId = "AddCity",
                IsAllowed = true
            },
            new()
            {
                UserId = 4,
                UseCaseId = "AddCountry",
                IsAllowed = true
            },
            new()
            {
                UserId = 5,
                UseCaseId = "UpdateCinema",
                IsAllowed = true
            },
            new()
            {
                UserId = 6,
                UseCaseId = "DeleteTheater",
                IsAllowed = false
            },
            new()
            {
                UserId = 6,
                UseCaseId = "UpdateTheater",
                IsAllowed = false
            },
            new()
            {
                UserId = 8,
                UseCaseId = "DeleteEmployeeMadeReservation",
                IsAllowed = false
            },
            new()
            {
                UserId = 9,
                UseCaseId = "UpdateScreening",
                IsAllowed = true
            },
            new()
            {
                UserId = 9,
                UseCaseId = "AddScreening",
                IsAllowed = true
            }
        };

        internal static List<Country> Countries => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                Name = "United States of America",
                Iso3Code = "USA"
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                Name = "United Kingdom",
                Iso3Code = "GBR"
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                Name = "Canada",
                Iso3Code = "CAN"
            },
            new()
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                Name = "Spain",
                Iso3Code = "ESP"
            },
            new()
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                Name = "France",
                Iso3Code = "FRA"
            },
            new()
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                Name = "Italy",
                Iso3Code = "ITA"
            }
        };

        internal static List<City> Cities => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                CountryId = 1,
                Name = "Los Angeles"
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                CountryId = 1,
                Name = "Chicago"
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                CountryId = 1,
                Name = "Boston"
            },
            new()
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                CountryId = 2,
                Name = "London"
            },
            new()
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                CountryId = 2,
                Name = "Manchester"
            },
            new()
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                CountryId = 3,
                Name = "Vancouver"
            },
            new()
            {
                Id = 7,
                CreatedAt = DateTime.UtcNow,
                CountryId = 3,
                Name = "Toronto"
            },
            new()
            {
                Id = 8,
                CreatedAt = DateTime.UtcNow,
                CountryId = 4,
                Name = "Sevilla"
            },
            new()
            {
                Id = 9,
                CreatedAt = DateTime.UtcNow,
                CountryId = 4,
                Name = "Madrid"
            },
            new()
            {
                Id = 10,
                CreatedAt = DateTime.UtcNow,
                CountryId = 6,
                Name = "Rome"
            },
            new()
            {
                Id = 11,
                CreatedAt = DateTime.UtcNow,
                CountryId = 6,
                Name = "Naples"
            }
        };

        internal static List<ArtistType> ArtistTypes => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                Name = "Actor"
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                Name = "Director"
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                Name = "Producer"
            },
            new()
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                Name = "Screenwriter"
            },
            new()
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                Name = "Cinematographer"
            },
            new()
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                Name = "Music Composer"
            }
        };

        internal static List<Artist> Artists => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1965, 3, 4),
                PlaceOfBirthId = 1,
                FirstName = "Mike",
                LastName = "Johns",
                Gender = Gender.Male
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1975, 3, 4),
                PlaceOfBirthId = 2,
                FirstName = "Mike",
                LastName = "Silver",
                Gender = Gender.Male
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1965, 3, 4),
                PlaceOfBirthId = 4,
                FirstName = "Anne",
                LastName = "Roberts",
                Gender = Gender.Female
            },
            new()
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(2000, 1, 4),
                PlaceOfBirthId = 6,
                FirstName = "Sarah",
                LastName = "Connors",
                Gender = Gender.Female
            },
            new()
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(2000, 1, 4),
                PlaceOfBirthId = 5,
                FirstName = "Lisa",
                LastName = "Walker",
                Gender = Gender.Female
            },
            new()
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1940, 1, 4),
                PlaceOfBirthId = 6,
                FirstName = "Peter",
                LastName = "Smith",
                Gender = Gender.Male
            },
            new()
            {
                Id = 7,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1970, 1, 4),
                PlaceOfBirthId = 8,
                FirstName = "Alejandro",
                LastName = "Lopez",
                Gender = Gender.Male
            },
            new()
            {
                Id = 8,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1990, 1, 4),
                PlaceOfBirthId = 8,
                FirstName = "Sofia",
                LastName = "Morales",
                Gender = Gender.Female
            },
            new()
            {
                Id = 9,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1990, 1, 4),
                PlaceOfBirthId = 10,
                FirstName = "Enco",
                LastName = "Ricci",
                Gender = Gender.Male
            },
            new()
            {
                Id = 10,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1980, 1, 4),
                PlaceOfBirthId = 11,
                FirstName = "Alessandra",
                LastName = "Colombo",
                Gender = Gender.Female
            },
            new()
            {
                Id = 11,
                CreatedAt = DateTime.UtcNow,
                DateOfBirth = new DateTime(1980, 1, 4),
                PlaceOfBirthId = 10,
                FirstName = "Francesca",
                LastName = "Marino",
                Gender = Gender.Female
            }
        };

        internal static List<Genre> Genres => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                Name = "Action"
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                Name = "Comedy"
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                Name = "Drama"
            },
            new()
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                Name = "Romance"
            },
            new()
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                Name = "Horror"
            },
            new()
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                Name = "Thriller"
            },
            new()
            {
                Id = 7,
                CreatedAt = DateTime.UtcNow,
                Name = "Science Fiction"
            },
            new()
            {
                Id = 8,
                CreatedAt = DateTime.UtcNow,
                Name = "Biography"
            },
            new()
            {
                Id = 9,
                CreatedAt = DateTime.UtcNow,
                Name = "Historical"
            },
            new()
            {
                Id = 10,
                CreatedAt = DateTime.UtcNow,
                Name = "Western"
            },
            new()
            {
                Id = 11,
                CreatedAt = DateTime.UtcNow,
                Name = "Satire"
            }
        };

        internal static List<Movie> Movies => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "Jaws",
                Runtime = 120,
                Description = "Movie description",
                ReleaseDate = new DateTime(2005, 3, 3),
                AvailableFrom = new DateTime(2019, 6, 6),
                AvailableUntill = new DateTime(2019, 8, 6)
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "Jaws 2",
                Runtime = 120,
                Description = "Movie description",
                ReleaseDate = new DateTime(2005, 3, 3),
                AvailableFrom = new DateTime(2021, 3, 6),
                AvailableUntill = new DateTime(2021, 8, 6)
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "Chinatown",
                Runtime = 105,
                Description = "Movie description",
                ReleaseDate = new DateTime(1970, 3, 3),
                AvailableFrom = new DateTime(2021, 3, 6),
                AvailableUntill = new DateTime(2021, 9, 6)
            },
            new()
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "Toy Story",
                Runtime = 75,
                Description = "Movie description",
                ReleaseDate = new DateTime(1990, 3, 3),
                AvailableFrom = new DateTime(2021, 3, 6),
                AvailableUntill = new DateTime(2021, 9, 6)
            },
            new()
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "The Dig",
                Runtime = 156,
                Description = "Movie description",
                ReleaseDate = new DateTime(2021, 6, 3),
                AvailableFrom = new DateTime(2021, 6, 6),
                AvailableUntill = new DateTime(2021, 9, 6)
            },
            new()
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "The Father",
                Runtime = 106,
                Description = "Movie description",
                ReleaseDate = new DateTime(2021, 6, 3),
                AvailableFrom = new DateTime(2021, 6, 5),
                AvailableUntill = new DateTime(2021, 7, 25)
            },
            new()
            {
                Id = 7,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "Concrete Cowboy",
                Runtime = 88,
                Description = "Movie description",
                ReleaseDate = new DateTime(2021, 4, 3),
                AvailableFrom = new DateTime(2021, 6, 5),
                AvailableUntill = new DateTime(2021, 7, 25)
            },
            new()
            {
                Id = 8,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "In the Heights",
                Runtime = 100,
                Description = "Movie description",
                ReleaseDate = new DateTime(2021, 4, 3),
                AvailableFrom = new DateTime(2021, 6, 5),
                AvailableUntill = new DateTime(2021, 7, 25)
            },
            new()
            {
                Id = 9,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "Zola",
                Runtime = 180,
                Description = "Movie description",
                ReleaseDate = new DateTime(2021, 4, 3),
                AvailableFrom = new DateTime(2021, 6, 5),
                AvailableUntill = new DateTime(2021, 7, 25)
            },
            new()
            {
                Id = 10,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "Old",
                Runtime = 150,
                Description = "Movie description",
                ReleaseDate = new DateTime(2021, 8, 1),
                AvailableFrom = new DateTime(2021, 9, 5),
                AvailableUntill = new DateTime(2021, 9, 25)
            },
            new()
            {
                Id = 11,
                CreatedAt = DateTime.UtcNow,
                Image = "4c4b9cda-2ffa-4f91-9315-8b7cd6b1f812.jpg",
                Title = "Candyman",
                Runtime = 99,
                Description = "Movie description",
                ReleaseDate = new DateTime(2021, 8, 1),
                AvailableFrom = new DateTime(2021, 9, 5),
                AvailableUntill = new DateTime(2021, 9, 25)
            }
        };

        internal static List<MovieGenre> MovieGenres => new()
        {
            new()
            {
                MovieId = 1,
                GenreId = 1
            },
            new()
            {
                MovieId = 1,
                GenreId = 2
            },
            new()
            {
                MovieId = 2,
                GenreId = 3
            },
            new()
            {
                MovieId = 3,
                GenreId = 4
            },
            new()
            {
                MovieId = 3,
                GenreId = 7
            },
            new()
            {
                MovieId = 4,
                GenreId = 7
            },
            new()
            {
                MovieId = 5,
                GenreId = 6
            },
            new()
            {
                MovieId = 7,
                GenreId = 8
            },
            new()
            {
                MovieId = 7,
                GenreId = 11
            },
            new()
            {
                MovieId = 8,
                GenreId = 10
            },
            new()
            {
                MovieId = 9,
                GenreId = 6
            },
            new()
            {
                MovieId = 10,
                GenreId = 4
            },
            new()
            {
                MovieId = 10,
                GenreId = 5
            },
            new()
            {
                MovieId = 11,
                GenreId = 6
            }
        };

        internal static List<Credit> Credits => new()
        {
            new()
            {
                MovieId = 1,
                ArtistId = 1,
                ArtistTypeId = 1
            },
            new()
            {
                MovieId = 1,
                ArtistId = 1,
                ArtistTypeId = 2
            },
            new()
            {
                MovieId = 1,
                ArtistId = 2,
                ArtistTypeId = 3
            },
            new()
            {
                MovieId = 4,
                ArtistId = 1,
                ArtistTypeId = 1
            },
            new()
            {
                MovieId = 7,
                ArtistId = 1,
                ArtistTypeId = 2
            },
            new()
            {
                MovieId = 7,
                ArtistId = 2,
                ArtistTypeId = 3
            },
            new()
            {
                MovieId = 9,
                ArtistId = 2,
                ArtistTypeId = 1
            },
            new()
            {
                MovieId = 6,
                ArtistId = 4,
                ArtistTypeId = 2
            },
            new()
            {
                MovieId = 6,
                ArtistId = 5,
                ArtistTypeId = 4
            },
            new()
            {
                MovieId = 6,
                ArtistId = 3,
                ArtistTypeId = 3
            },
            new()
            {
                MovieId = 10,
                ArtistId = 7,
                ArtistTypeId = 2
            },
            new()
            {
                MovieId = 11,
                ArtistId = 5,
                ArtistTypeId = 4
            }
        };

        internal static List<Cinema> Cinemas => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                CityId = 1,
                Name = "Apollo",
                Address = "Coast Blvd 760",
                PhoneNumber = "+1 60 1234 505"
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                CityId = 2,
                Name = "Star",
                Address = "Lincoln street 150",
                PhoneNumber = "+1 60 1234 506"
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                CityId = 3,
                Name = "Star",
                Address = "Lincoln street 150",
                PhoneNumber = "+1 60 1234 507"
            }
        };

        internal static List<Theater> Theaters => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                CinemaId = 1,
                Name = "Theater 1",
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                CinemaId = 1,
                Name = "Theater 2",
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                CinemaId = 1,
                Name = "Theater 3",
            },
            new()
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                CinemaId = 2,
                Name = "Humphrey Bogart",
            },
            new()
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                CinemaId = 2,
                Name = "Rita Hayworth",
            },
            new()
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                CinemaId = 3,
                Name = "Theater 1",
            },
            new()
            {
                Id = 7,
                CreatedAt = DateTime.UtcNow,
                CinemaId = 3,
                Name = "Theater 2",
            }
        };

        internal static List<Seat> Seats
        {
            get
            {
                List<Seat> list = new();

                list.GenerateSeats(1, 5, 10);
                list.GenerateSeats(2, 4, 8);
                list.GenerateSeats(3, 6, 5);
                list.GenerateSeats(4, 6, 7);
                list.GenerateSeats(5, 6, 4);
                list.GenerateSeats(6, 8, 8);
                list.GenerateSeats(7, 10, 6);

                return list;
            }
        }

        internal static List<Screening> Screenings => new()
        {
            new()
            {
                Id = 1,
                CreatedAt = DateTime.UtcNow,
                MovieId = 1,
                TheaterId = 1,
                Price = 20,
                ScreeningStartsAt = new DateTime(2019, 7, 6, 20, 0, 0)
            },
            new()
            {
                Id = 2,
                CreatedAt = DateTime.UtcNow,
                MovieId = 1,
                TheaterId = 2,
                Price = 15,
                ScreeningStartsAt = new DateTime(2019, 7, 8, 15, 30, 0)
            },
            new()
            {
                Id = 3,
                CreatedAt = DateTime.UtcNow,
                MovieId = 3,
                TheaterId = 3,
                Price = 25,
                ScreeningStartsAt = new DateTime(2021, 3, 15, 21, 0, 0)
            },
            new()
            {
                Id = 4,
                CreatedAt = DateTime.UtcNow,
                MovieId = 3,
                TheaterId = 2,
                Price = 10,
                ScreeningStartsAt = new DateTime(2021, 3, 15, 16, 0, 0)
            },
            new()
            {
                Id = 5,
                CreatedAt = DateTime.UtcNow,
                MovieId = 4,
                TheaterId = 1,
                Price = 20,
                ScreeningStartsAt = new DateTime(2021, 8, 7, 19, 0, 0)
            },
            new()
            {
                Id = 6,
                CreatedAt = DateTime.UtcNow,
                MovieId = 4,
                TheaterId = 6,
                Price = 20,
                ScreeningStartsAt = new DateTime(2021, 8, 7, 23, 0, 0)
            },
            new()
            {
                Id = 7,
                CreatedAt = DateTime.UtcNow,
                MovieId = 6,
                TheaterId = 2,
                Price = 30,
                ScreeningStartsAt = new DateTime(2021, 7, 15, 18, 0, 0)
            },
            new()
            {
                Id = 8,
                CreatedAt = DateTime.UtcNow,
                MovieId = 6,
                TheaterId = 3,
                Price = 25,
                ScreeningStartsAt = new DateTime(2021, 7, 15, 22, 0, 0)
            },
            new()
            {
                Id = 9,
                CreatedAt = DateTime.UtcNow,
                MovieId = 9,
                TheaterId = 2,
                Price = 20,
                ScreeningStartsAt = new DateTime(2021, 9, 18, 20, 0, 0)
            },
            new()
            {
                Id = 10,
                CreatedAt = DateTime.UtcNow,
                MovieId = 9,
                TheaterId = 5,
                Price = 20,
                ScreeningStartsAt = new DateTime(2021, 9, 18, 20, 0, 0)
            },
            new()
            {
                Id = 11,
                CreatedAt = DateTime.UtcNow,
                MovieId = 8,
                TheaterId = 4,
                Price = 20,
                ScreeningStartsAt = new DateTime(2021, 6, 22, 16, 0, 0)
            }
        };

        internal static List<Reservation> Reservations => new()
        {
            new()
            {
                Id = 1,
                ScreeningId = 1,
                CreatedAt = DateTime.UtcNow,
                CustomerId = 12,
                IsReserved = true,
                IsPaid = true,
                Email = "sarah.wright.87@gmail.com",
                FirstName = "Sarah",
                LastName = "Wright",
                PhoneNumber = "+1 60 9876 600"
            },
            new()
            {
                Id = 2,
                ScreeningId = 2,
                CreatedAt = DateTime.UtcNow,
                CustomerId = 12,
                IsReserved = true,
                IsPaid = true,
                Email = "sarah.wright.87@gmail.com",
                FirstName = "Sarah",
                LastName = "Wright",
                PhoneNumber = "+1 60 9876 600"
            },
            new()
            {
                Id = 3,
                ScreeningId = 7,
                CreatedAt = DateTime.UtcNow,
                CustomerId = 12,
                IsReserved = true,
                IsPaid = true,
                Email = "sarah.wright.87@gmail.com",
                FirstName = "Sarah",
                LastName = "Wright",
                PhoneNumber = "+1 60 9876 600"
            },
            new()
            {
                Id = 4,
                ScreeningId = 7,
                CreatedAt = DateTime.UtcNow,
                CustomerId = 14,
                IsReserved = true,
                IsPaid = true,
                Email = "daniel.walker@gmail.com",
                FirstName = "Daniel",
                LastName = "Walker",
                PhoneNumber = "+1 60 9872 604"
            },
            new()
            {
                Id = 5,
                ScreeningId = 4,
                CreatedAt = DateTime.UtcNow,
                CustomerId = 12,
                IsReserved = true,
                IsPaid = true,
                Email = "sarah.wright.87@gmail.com",
                FirstName = "Sarah",
                LastName = "Wright",
                PhoneNumber = "+1 60 9876 600"
            },
            new()
            {
                Id = 6,
                ScreeningId = 8,
                CreatedAt = DateTime.UtcNow,
                CustomerId = 14,
                IsReserved = true,
                IsPaid = true,
                Email = "daniel.walker@gmail.com",
                FirstName = "Daniel",
                LastName = "Walker",
                PhoneNumber = "+1 60 9872 604"
            },
            new()
            {
                Id = 7,
                ScreeningId = 8,
                CreatedAt = DateTime.UtcNow,
                CustomerId = null,
                IsReserved = true,
                IsPaid = false,
                Email = "johny34@gmail.com",
                FirstName = "John",
                PhoneNumber = "+1 60 9872 677"
            },
            new()
            {
                Id = 8,
                ScreeningId = 4,
                CreatedAt = DateTime.UtcNow,
                CustomerId = null,
                IsReserved = false,
                IsPaid = true,
            },
            new()
            {
                Id = 9,
                ScreeningId = 3,
                CreatedAt = DateTime.UtcNow,
                CustomerId = null,
                IsReserved = false,
                IsPaid = true,
            },
            new()
            {
                Id = 10,
                ScreeningId = 9,
                CreatedAt = DateTime.UtcNow,
                CustomerId = null,
                IsReserved = true,
                IsPaid = false,
                PhoneNumber = "+1 62 1172 677"
            },
            new()
            {
                Id = 11,
                ScreeningId = 10,
                CreatedAt = DateTime.UtcNow,
                CustomerId = 14,
                IsReserved = true,
                IsPaid = false,
                Email = "daniel.walker@gmail.com",
                FirstName = "Daniel",
                LastName = "Walker",
                PhoneNumber = "+1 60 9872 604"
            }
        };

        internal static List<ReservedSeat> ReservedSeats => new()
        {
            new()
            {
                SeatId = 1,
                ScreeningId = 1,
                ReservationId = 1
            },
            new()
            {
                SeatId = 2,
                ScreeningId = 1,
                ReservationId = 1
            },
            new()
            {
                SeatId = 3,
                ScreeningId = 1,
                ReservationId = 1
            },
            new()
            {
                SeatId = 40,
                ScreeningId = 2,
                ReservationId = 2
            },
            new()
            {
                SeatId = 41,
                ScreeningId = 2,
                ReservationId = 2
            },
            new()
            {
                SeatId = 42,
                ScreeningId = 2,
                ReservationId = 2
            },
            new()
            {
                SeatId = 40,
                ScreeningId = 7,
                ReservationId = 3
            },
            new()
            {
                SeatId = 41,
                ScreeningId = 7,
                ReservationId = 4
            },
            new()
            {
                SeatId = 50,
                ScreeningId = 4,
                ReservationId = 5
            },
            new()
            {
                SeatId = 70,
                ScreeningId = 8,
                ReservationId = 6
            },
            new()
            {
                SeatId = 71,
                ScreeningId = 8,
                ReservationId = 6
            },
            new()
            {
                SeatId = 72,
                ScreeningId = 8,
                ReservationId = 7
            },
            new()
            {
                SeatId = 48,
                ScreeningId = 4,
                ReservationId = 8
            },
            new()
            {
                SeatId = 60,
                ScreeningId = 3,
                ReservationId = 9
            },
            new()
            {
                SeatId = 45,
                ScreeningId = 9,
                ReservationId = 10
            },
            new()
            {
                SeatId = 46,
                ScreeningId = 9,
                ReservationId = 10
            },
            new()
            {
                SeatId = 120,
                ScreeningId = 10,
                ReservationId = 11
            },
            new()
            {
                SeatId = 121,
                ScreeningId = 10,
                ReservationId = 11
            }
        };

        private static List<RoleUseCase> GenerateRoleUseCases(this List<RoleUseCase> list, int roleId, string resource)
        {
            new List<string> { "Add", "Update", "Delete" }
                .ForEach(x => list.Add(new RoleUseCase { RoleId = roleId, UseCaseId = $"{x}{resource}" }));

            list.Add(GenerateSearchUseCase(roleId, resource));

            return list;
        }

        private static RoleUseCase GenerateSearchUseCase(int roleId, string resource)
        {
            if (resource.EndsWith("y"))
            {
                resource = resource.Remove(resource.Length - 1) + "ie";
            }

            return new RoleUseCase { RoleId = roleId, UseCaseId = $"Search{resource}s" };
        }

        private static List<Seat> GenerateSeats(this List<Seat> list, int theaterId, int rows, int seatsPerRow)
        {
            int id = list.Any() ? list.Last().Id : 0;

            for (int x = 1; x < rows; x++)
            {
                for (int y = 1; y < seatsPerRow; y++)
                {
                    list.Add(new Seat
                    {
                        Id = ++id,
                        TheaterId = theaterId,
                        CreatedAt = DateTime.UtcNow,
                        Row = x,
                        Number = y
                    });
                }
            }

            return list;
        }
    }
}