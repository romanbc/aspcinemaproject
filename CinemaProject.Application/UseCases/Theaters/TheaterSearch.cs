﻿namespace CinemaProject.Application.UseCases.Theaters
{
    public class TheaterSearch : PagedSearch
    {
        public string Name { get; set; }
        public string Cinema { get; set; }
    }
}