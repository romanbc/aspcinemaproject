﻿using CinemaProject.Application.UseCases.Seats;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Seats
{
    public class SeatValidator : AbstractValidator<SeatDto>
    {
        public SeatValidator(CinemaProjectContext context)
        {
            RuleFor(x => x.Row)
                .NotEmpty()
                .InclusiveBetween(1, 200);

            RuleFor(x => x.Number)
                .NotEmpty()
                .InclusiveBetween(1, 500);

            RuleFor(x => x.TheaterId)
                .NotEmpty()
                .GreaterThan(0)
                .Must(x => context.Theaters.Any(t => t.Id == x))
                .WithMessage("Theater with that id does not exist");
        }
    }
}