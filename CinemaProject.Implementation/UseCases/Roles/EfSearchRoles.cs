﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Roles;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Roles
{
    public class EfSearchRoles : EfGenericSearch<Role, RoleDto, RoleSearch>, ISearchRoles
    {
        private readonly CinemaProjectContext _context;

        public EfSearchRoles(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchRoles";

        public override PagedResponse<RoleDto> Execute(RoleSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x => x.Name.ToLower().Contains(search.Name.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.UseCase),
                    PerformFilter = x => x.Where(x => x.RoleUseCases.Any(ruc => ruc.UseCaseId.ToLower().Contains(search.UseCase.ToLower())))
                },
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<Role>> Query => () => _context.Roles.Include(x => x.RoleUseCases);
    }
}