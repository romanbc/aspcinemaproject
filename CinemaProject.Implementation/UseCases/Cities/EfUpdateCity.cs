﻿using AutoMapper;
using CinemaProject.Application.UseCases.Cities;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Cities
{
    public class EfUpdateCity : EfGenericUpdate<CityDto, City>, IUpdateCity
    {
        public EfUpdateCity(CinemaProjectContext context, IMapper mapper, UpdateCityValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateCity";
    }
}