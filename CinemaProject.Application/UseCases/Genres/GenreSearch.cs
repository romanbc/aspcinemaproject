﻿namespace CinemaProject.Application.UseCases.Genres
{
    public class GenreSearch : PagedSearch
    {
        public string Name { get; set; }
    }
}