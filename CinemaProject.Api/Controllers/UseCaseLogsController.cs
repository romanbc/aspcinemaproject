﻿using CinemaProject.Application;
using CinemaProject.Application.UseCases.UseCaseLogs;
using Microsoft.AspNetCore.Mvc;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UseCaseLogsController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;
        public UseCaseLogsController(UseCaseExecutor executor)
        {
            _executor = executor;
        }

        [HttpGet]
        public IActionResult Get([FromQuery] UseCaseLogSearch search, [FromServices] ISearchUseCaseLogs query)
        {
            return Ok(_executor.ExecuteQuery(query, search));
        }
    }
}
