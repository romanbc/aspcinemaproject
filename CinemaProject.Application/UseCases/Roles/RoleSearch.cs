﻿namespace CinemaProject.Application.UseCases.Roles
{
    public class RoleSearch : PagedSearch
    {
        public string Name { get; set; }
        public string UseCase { get; set; }
    }
}