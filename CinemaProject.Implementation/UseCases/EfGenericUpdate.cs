﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases
{
    public abstract class EfGenericUpdate<TDto, TEntity> : ICommand<TDto>
       where TEntity : BaseEntity
       where TDto : BaseDto
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<TDto> _validator;

        protected EfGenericUpdate(CinemaProjectContext context, IMapper mapper, AbstractValidator<TDto> validator)
        {
            _context = context;
            _mapper = mapper;
            _validator = validator;
        }

        public void Execute(TDto request)
        {
            var item = _context.Set<TEntity>().Find(request.Id);

            if (item == null)
            {
                throw new EntityNotFoundException(request.Id, typeof(TEntity));
            }

            _validator.ValidateAndThrow(request);
            _mapper.Map(request, item);

            _context.SaveChanges();
        }

        public abstract string Id { get; }
    }
}