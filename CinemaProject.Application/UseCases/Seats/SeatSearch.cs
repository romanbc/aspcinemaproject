﻿namespace CinemaProject.Application.UseCases.Seats
{
    public class SeatSearch : PagedSearch
    {
        public int? Row { get; set; }
        public int? Number { get; set; }
        public int? TheaterId { get; set; }
    }
}