﻿namespace CinemaProject.Application.UseCases.Screenings
{
    public interface IFindScreening : IQuery<int, ReadScreeningDto>
    {
    }
}