﻿using CinemaProject.Application.UseCases.Genres;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.UseCases.Genres;
using FluentAssertions;
using FluentValidation;
using Moq;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CinemaProject.Tests
{
    public class ValidationTestsForGenre : IClassFixture<GenreFixture>
    {
        private GenreFixture Fixture { get; }

        public ValidationTestsForGenre(GenreFixture fixture)
        {
            Fixture = fixture;
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void ValidationReturnsError_WhenEmptyName(string name)
        {
            var dbMock = new Mock<CinemaProjectContext>();
            var validator = new AddGenreValidator(dbMock.Object);
            var dto = new GenreDto
            {
                Name = name
            };

            var result = validator.Validate(dto);

            result.IsValid.Should().BeFalse();
            result.Errors.Where(x => x.PropertyName == "Name").Should().NotBeEmpty();
            result.Errors.First(x => x.PropertyName == "Name").ErrorMessage.Should()
                .Be("Name is required.");
        }

        [Fact]
        public void ValidationError_WhenNameIsInUse()
        {
            var dbMock = Fixture.DbMock;

            var validator = new AddGenreValidator(dbMock.Object);
            var dto = new GenreDto
            {
                Name = "Comedy"
            };

            var result = validator.Validate(dto);

            result.Errors.Where(x => x.PropertyName == "Name").Should().NotBeEmpty();
            result.Errors.First(x => x.PropertyName == "Name").ErrorMessage.Should()
                .Be("Name is already taken.");
        }

        [Fact]
        public void ValidationError_WhenUpdating_WhenNameIsInUse()
        {
            var dbMock = Fixture.DbMock;

            var validator = new UpdateGenreValidator(dbMock.Object);
            var dto = new GenreDto
            {
                Id = 1,
                Name = "Action"
            };

            var result = validator.Validate(dto);

            result.Errors.Where(x => x.PropertyName == "Name").Should().NotBeEmpty();
            result.Errors.First(x => x.PropertyName == "Name").ErrorMessage.Should()
                .Be("Name is already taken.");
        }

        [Theory]
        [InlineData("Comedy")]
        [InlineData("Sci-fi")]
        public void ValidationOk_WhenUpdating_WhenNameIsNotInUse(string name)
        {
            var dbMock = Fixture.DbMock;

            var validator = new UpdateGenreValidator(dbMock.Object);
            var dto = new GenreDto
            {
                Id = 1,
                Name = name
            };

            var result = validator.Validate(dto);

            result.Errors.Where(x => x.PropertyName == "Name").Should().BeEmpty();
        }
    }

    public class GenreFixture
    {
        public Mock<CinemaProjectContext> DbMock { get; private set; }

        public IEnumerable<Genre> Genres { get; set; } = new List<Genre>
        {
            new Genre
            {
                Id = 1,
                Name = "Comedy"
            },
            new Genre
            {
                Id = 2,
                Name = "Action"
            }
        };

        public GenreFixture()
        {
            SetupMock();
        }

        private void SetupMock()
        {
            var genreDbSet = Genres.GetQueryableMockDbSet();

            foreach (var genre in Genres)
            {
                genreDbSet.Add(genre);
            }

            DbMock = new Mock<CinemaProjectContext>();

            DbMock.Setup(x => x.Set<Genre>()).Returns(genreDbSet);
            DbMock.Setup(x => x.Genres).Returns(genreDbSet);
        }
    }
}