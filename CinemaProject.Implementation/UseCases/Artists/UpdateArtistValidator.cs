﻿using CinemaProject.Application.UseCases.Artists;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Artists
{
    public class UpdateArtistValidator : AbstractValidator<ArtistDto>
    {
        public UpdateArtistValidator(CinemaProjectContext context)
        {
            Include(new ArtistValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Artists.Any(a =>
                    a.FirstName == x.FirstName
                    && a.LastName == x.LastName
                    && a.PlaceOfBirthId == x.PlaceOfBirthId
                    && a.DateOfBirth == x.DateOfBirth
                    && a.Id != x.Id
                    )
                )
                .WithMessage("Artist with the same name, born on the same day in the same city already exists");
        }
    }
}