﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.ArtistTypes
{
    public class EfSearchArtistTypes : EfGenericSearch<ArtistType, ArtistTypeDto, ArtistTypeSearch>, ISearchArtistTypes
    {
        public EfSearchArtistTypes(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public override string Id => "SearchArtistTypes";

        public override PagedResponse<ArtistTypeDto> Execute(ArtistTypeSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x => x.Name.ToLower().Contains(search.Name.ToLower()))
                }
            };

            return Execute(search, filters);
        }
    }
}