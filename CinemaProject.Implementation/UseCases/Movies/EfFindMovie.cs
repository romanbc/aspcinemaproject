﻿using AutoMapper;
using CinemaProject.Application.UseCases.Movies;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Movies
{
    public class EfFindMovie : EfGenericFind<Movie, ReadMovieDto>, IFindMovie
    {
        private readonly CinemaProjectContext _context;

        public EfFindMovie(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindMovie";

        public override string ChildOf => "SearchMovies";

        protected override Func<IQueryable<Movie>> Query => () => _context.Movies
            .Include(x => x.MovieGenres)
                .ThenInclude(x => x.Genre)
            .Include(x => x.Credits)
                .ThenInclude(x => x.Artist)
            .Include(x => x.Credits)
                .ThenInclude(x => x.ArtistType)
            .AsSplitQuery();
    }
}