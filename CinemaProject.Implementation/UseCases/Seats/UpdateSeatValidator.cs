﻿using CinemaProject.Application.UseCases.Seats;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Seats
{
    public class UpdateSeatValidator : AbstractValidator<SeatDto>
    {
        public UpdateSeatValidator(CinemaProjectContext context)
        {
            Include(new SeatValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Seats.Any(s =>
                        s.Row == x.Row
                        && s.Number == x.Number
                        && s.TheaterId == x.TheaterId
                        && s.Id != x.Id
                    )
                )
                .WithMessage("Theater already has seat with the same row and number");
        }
    }
}