﻿using CinemaProject.Domain.Entities;
using CinemaProject.Domain.Relations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace CinemaProject.EfDataAccess
{
    internal static class ModelBuilderExtensions
    {
        internal static void SetGlobalQueryFilter(this ModelBuilder modelBuilder, Expression<Func<BaseEntity, bool>> filterExpr)
        {
            foreach (var mutableEntityType in modelBuilder.Model.GetEntityTypes())
            {
                if (mutableEntityType.ClrType.IsAssignableTo(typeof(BaseEntity)))
                {
                    var parameter = Expression.Parameter(mutableEntityType.ClrType);
                    var body = ReplacingExpressionVisitor.Replace(filterExpr.Parameters.First(), parameter, filterExpr.Body);
                    var lambdaExpression = Expression.Lambda(body, parameter);

                    mutableEntityType.SetQueryFilter(lambdaExpression);
                }
            }
        }

        internal static void SetupInitialData(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(InitialContextData.Roles);
            modelBuilder.Entity<RoleUseCase>().HasData(InitialContextData.RoleUseCases);
            modelBuilder.Entity<User>().HasData(InitialContextData.Users);
            modelBuilder.Entity<UserRole>().HasData(InitialContextData.UserRoles);
            modelBuilder.Entity<UserUseCase>().HasData(InitialContextData.UserUseCases);

            modelBuilder.Entity<City>().HasData(InitialContextData.Cities);
            modelBuilder.Entity<Country>().HasData(InitialContextData.Countries);

            modelBuilder.Entity<Artist>().HasData(InitialContextData.Artists);
            modelBuilder.Entity<ArtistType>().HasData(InitialContextData.ArtistTypes);

            modelBuilder.Entity<Genre>().HasData(InitialContextData.Genres);
            modelBuilder.Entity<Movie>().HasData(InitialContextData.Movies);
            modelBuilder.Entity<MovieGenre>().HasData(InitialContextData.MovieGenres);
            modelBuilder.Entity<Credit>().HasData(InitialContextData.Credits);

            modelBuilder.Entity<Cinema>().HasData(InitialContextData.Cinemas);
            modelBuilder.Entity<Theater>().HasData(InitialContextData.Theaters);
            modelBuilder.Entity<Seat>().HasData(InitialContextData.Seats);

            modelBuilder.Entity<Screening>().HasData(InitialContextData.Screenings);
            modelBuilder.Entity<Reservation>().HasData(InitialContextData.Reservations);
            modelBuilder.Entity<ReservedSeat>().HasData(InitialContextData.ReservedSeats);
        }
    }
}