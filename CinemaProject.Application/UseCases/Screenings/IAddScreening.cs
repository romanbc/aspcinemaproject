﻿namespace CinemaProject.Application.UseCases.Screenings
{
    public interface IAddScreening : ICommand<ScreeningDto>
    {
    }
}