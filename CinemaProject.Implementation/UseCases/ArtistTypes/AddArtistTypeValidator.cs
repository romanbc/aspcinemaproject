﻿using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.ArtistTypes
{
    public class AddArtistTypeValidator : AbstractValidator<ArtistTypeDto>
    {
        public AddArtistTypeValidator(CinemaProjectContext context)
        {
            Include(new ArtistTypeValidator(context));
        }
    }
}