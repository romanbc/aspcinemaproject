﻿using AutoMapper;
using CinemaProject.Application.UseCases.Seats;
using CinemaProject.Domain.Entities;

namespace CinemaProject.Implementation.Profiles
{
    public class SeatProfile : Profile
    {
        public SeatProfile()
        {
            CreateMap<Seat, SeatDto>().ReverseMap();
        }
    }
}