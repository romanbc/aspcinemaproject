﻿namespace CinemaProject.Application.UseCases.Cinemas
{
    public class CinemaSearch : PagedSearch
    {
        public string Name { get; set; }
        public string Location { get; set; }
    }
}