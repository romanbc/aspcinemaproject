﻿using AutoMapper;
using CinemaProject.Application.UseCases.Artists;
using CinemaProject.Domain.Entities;

namespace CinemaProject.Implementation.Profiles
{
    public class ArtistProfile : Profile
    {
        public ArtistProfile()
        {
            CreateMap<Artist, ArtistDto>()
                .ForMember(dto => dto.PlaceOfBirth, o =>
                    o.MapFrom(a => a.PlaceOfBirth.Name + ", " + a.PlaceOfBirth.Country.Name))
                .ForMember(dto => dto.GenderName, o => o.MapFrom(a => a.Gender.ToString()));

            CreateMap<ArtistDto, Artist>()
                .ForMember(a => a.PlaceOfBirth, o => o.Ignore());
        }
    }
}