﻿namespace CinemaProject.Application.UseCases.Genres
{
    public interface IFindGenre : IQuery<int, GenreDto>
    {
    }
}