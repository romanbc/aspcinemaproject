﻿using CinemaProject.Domain.Relations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaProject.Domain.Entities
{
    public class Movie : BaseEntity
    {
        public string Title { get; set; }

        [Column(TypeName = "Date")]
        public DateTime ReleaseDate { get; set; }

        public string Description { get; set; }
        public int Runtime { get; set; }

        [Column(TypeName = "Date")]
        public DateTime AvailableFrom { get; set; }

        [Column(TypeName = "Date")]
        public DateTime AvailableUntill { get; set; }

        public string Image { get; set; }

        public virtual ICollection<MovieGenre> MovieGenres { get; set; } = new HashSet<MovieGenre>();
        public virtual ICollection<Credit> Credits { get; set; } = new HashSet<Credit>();
        public virtual ICollection<Screening> Screenings { get; set; } = new HashSet<Screening>();
    }
}