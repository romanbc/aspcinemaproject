﻿using CinemaProject.Application.UseCases.Users;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Users
{
    public class CheckForSuperAdminPrivilegesValidator : AbstractValidator<UserDto>
    {
        public CheckForSuperAdminPrivilegesValidator(CinemaProjectContext context)
        {
            RuleForEach(x => x.RoleIds)
                       .Must(x => !context.Roles.Any(r => r.Id == x && r.Name == "SuperAdmin"))
                       .WithMessage("You don't have permission to create a SuperAdmin user");

            RuleForEach(x => x.UserSpecificUseCases)
                      .Must(x => !(x.IsAllowed && UseCaseConfig.SuperAdminExclusiveUseCaseIds.Contains(x.UseCaseId)))
                      .WithMessage((y, x) => $"Use case Id '{x.UseCaseId}' is allowed only to the SuperAdmin users. You don't have necessary permissions for this operation");
        }
    }
}