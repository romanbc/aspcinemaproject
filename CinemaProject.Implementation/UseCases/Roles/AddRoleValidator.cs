﻿using CinemaProject.Application.UseCases.Roles;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Roles
{
    public class AddRoleValidator : AbstractValidator<RoleDto>
    {
        public AddRoleValidator(CinemaProjectContext context)
        {
            Include(new RoleValidator(context));
        }
    }
}