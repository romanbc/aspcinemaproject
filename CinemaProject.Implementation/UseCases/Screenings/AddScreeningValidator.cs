﻿using CinemaProject.Application.UseCases.Screenings;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Screenings
{
    public class AddScreeningValidator : AbstractValidator<ScreeningDto>
    {
        public AddScreeningValidator(CinemaProjectContext context)
        {
            Include(new ScreeningValidator(context));

            RuleFor(x => x)
                .Must(x => !context.Screenings.Any(s =>
                        s.TheaterId == x.TheaterId
                        && s.ScreeningStartsAt.AddMinutes(s.Movie.Runtime + 15) >= x.ScreeningStartsAt
                        && s.ScreeningStartsAt <= x.ScreeningStartsAt
                    )
                )
                .WithMessage("Theater is occupied at this time, please select another free time slot and bear in mind that there should be at least 15 minutes in between of two screenings");
        }
    }
}