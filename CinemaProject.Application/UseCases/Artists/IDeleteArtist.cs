﻿namespace CinemaProject.Application.UseCases.Artists
{
    public interface IDeleteArtist : ICommand<int>
    {
    }
}
