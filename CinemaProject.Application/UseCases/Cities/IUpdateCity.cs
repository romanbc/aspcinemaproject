﻿namespace CinemaProject.Application.UseCases.Cities
{
    public interface IUpdateCity : ICommand<CityDto>
    {
    }
}