﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    internal class GenreConfiguration : IEntityTypeConfiguration<Genre>
    {
        public void Configure(EntityTypeBuilder<Genre> builder)
        {
            //builder.HasIndex(g => g.Name).IsUnique();
            builder.Property(g => g.Name).IsRequired().HasMaxLength(100);
        }
    }
}