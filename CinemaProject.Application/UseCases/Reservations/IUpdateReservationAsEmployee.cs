﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IUpdateReservationAsEmployee : ICommand<ReservationDto>
    {
    }
}