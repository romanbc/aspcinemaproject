﻿using CinemaProject.Application.UseCases.Screenings;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Screenings
{
    public class ScreeningValidator : AbstractValidator<ScreeningDto>
    {
        public ScreeningValidator(CinemaProjectContext context)
        {
            RuleFor(x => x.Price)
                .InclusiveBetween(0, 5000);

            RuleFor(x => x.MovieId)
                .NotEmpty()
                .GreaterThan(0)
                .Must(x => context.Movies.Any(t => t.Id == x))
                .WithMessage("Movie with that id does not exist");

            RuleFor(x => x.TheaterId)
                .NotEmpty()
                .GreaterThan(0)
                .Must(x => context.Theaters.Any(t => t.Id == x))
                .WithMessage("Theater with that id does not exist");

            RuleFor(x => x.ScreeningStartsAt)
                .NotEmpty()
                .GreaterThan(DateTime.UtcNow)
                .Must((y, x) => !context.Movies
                    .Any(m =>
                        m.Id == y.MovieId
                        && (m.AvailableFrom > x.Date || m.AvailableUntill < x.Date)
                    )
                )
                .WithMessage((y, x) =>
                {
                    var movie = context.Movies.Find(y.MovieId);
                    return $"Movie is available for screening from {movie.AvailableFrom.ToShortDateString()} to {movie.AvailableUntill.ToShortDateString()}";
                });
        }
    }
}