﻿using CinemaProject.Application.UseCases.ArtistTypes;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.ArtistTypes
{
    public class ArtistTypeValidator : AbstractValidator<ArtistTypeDto>
    {
        public ArtistTypeValidator(CinemaProjectContext context, bool isUpdate = false)
        {
            RuleFor(x => x.Name)
                .NameValidation()
                .CheckIfAlreadyExists<ArtistTypeDto, ArtistType>(context, "Name", isUpdate);
        }
    }
}