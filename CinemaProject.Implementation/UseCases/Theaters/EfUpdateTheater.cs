﻿using AutoMapper;
using CinemaProject.Application.UseCases.Theaters;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Theaters
{
    public class EfUpdateTheater : EfGenericUpdate<TheaterDto, Theater>, IUpdateTheater
    {
        public EfUpdateTheater(CinemaProjectContext context, IMapper mapper, UpdateTheaterValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateTheater";
    }
}