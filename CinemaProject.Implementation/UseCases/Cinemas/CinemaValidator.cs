﻿using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cinemas
{
    public class CinemaValidator : AbstractValidator<CinemaDto>
    {
        public CinemaValidator(CinemaProjectContext context)
        {
            RuleFor(x => x.Name)
                .TitleValidation();

            RuleFor(x => x.Address)
                .TitleValidation();

            RuleFor(x => x.PhoneNumber)
                .PhoneNumberValidation()
                .When(x => !string.IsNullOrWhiteSpace(x.PhoneNumber));

            RuleFor(x => x.CityId)
                .NotEmpty()
                .Must(x => context.Cities.Any(c => c.Id == x))
                .WithMessage("City with an id of {PropertyValue} does not exist");
        }
    }
}