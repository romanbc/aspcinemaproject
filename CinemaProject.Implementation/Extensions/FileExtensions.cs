﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;

namespace CinemaProject.Implementation.Extensions
{
    public static class FileExtensions
    {
        public static string UploadFile(this IFormFile image, string folder)
        {
            var guid = Guid.NewGuid();

            var extension = Path.GetExtension(image.FileName);

            var newFileName = guid + extension;

            var path = Path.Combine("wwwroot", folder, newFileName);

            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                image.CopyTo(fileStream);
            }

            return newFileName;
        }
    }
}