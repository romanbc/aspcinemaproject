﻿namespace CinemaProject.Application.UseCases.Movies
{
    public interface IFindMovie : IQuery<int, ReadMovieDto>
    {
    }
}