﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Cities;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cities
{
    public class EfSearchCities : EfGenericSearch<City, CityDto, CitySearch>, ISearchCities
    {
        private readonly CinemaProjectContext _context;

        public EfSearchCities(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchCities";

        public override PagedResponse<CityDto> Execute(CitySearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x => x.Name.ToLower().Contains(search.Name.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Country),
                    PerformFilter = x => x.Where(x =>
                        x.Country.Name.ToLower().Contains(search.Country.ToLower())
                        || x.Country.Iso3Code.ToLower().Contains(search.Country.ToLower())
                    )
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<City>> Query => () => _context.Cities.Include(x => x.Country);
    }
}