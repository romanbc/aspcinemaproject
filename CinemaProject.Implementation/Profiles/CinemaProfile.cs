﻿using AutoMapper;
using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.Domain.Entities;
using System.Linq;

namespace CinemaProject.Implementation.Profiles
{
    public class CinemaProfile : Profile
    {
        public CinemaProfile()
        {
            CreateMap<Cinema, CinemaDto>()
                .ForMember(dto => dto.City, o => o.MapFrom(c => c.City.Name + ", " + c.City.Country.Name))
                .ForMember(dto => dto.Theaters, o => o.MapFrom(c => c.Theaters.Select(t => t.Name)));

            CreateMap<CinemaDto, Cinema>()
                .ForMember(c => c.City, o => o.Ignore())
                .ForMember(c => c.Theaters, o => o.Ignore());
        }
    }
}