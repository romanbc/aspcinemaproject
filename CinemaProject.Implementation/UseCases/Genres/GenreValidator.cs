﻿using CinemaProject.Application.UseCases.Genres;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Genres
{
    public class GenreValidator : AbstractValidator<GenreDto>
    {
        public GenreValidator(CinemaProjectContext context, bool isUpdate = false)
        {
            CascadeMode = CascadeMode.Stop;

            RuleFor(x => x.Name)
                .TitleValidation()
                .CheckIfAlreadyExists<GenreDto, Genre>(context, "Name", isUpdate);
        }
    }
}