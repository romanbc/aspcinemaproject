﻿namespace CinemaProject.Application.UseCases.Countries
{
    public interface IAddCountry : ICommand<CountryDto>
    {
    }
}