﻿namespace CinemaProject.Application.UseCases.Seats
{
    public interface ISearchSeats : IQuery<SeatSearch, PagedResponse<SeatDto>>
    {
    }
}