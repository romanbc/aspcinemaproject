﻿using CinemaProject.Application.UseCases.Roles;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Roles
{
    public class UpdateRoleValidator : AbstractValidator<RoleDto>
    {
        public UpdateRoleValidator(CinemaProjectContext context)
        {
            Include(new RoleValidator(context, true));
        }
    }
}