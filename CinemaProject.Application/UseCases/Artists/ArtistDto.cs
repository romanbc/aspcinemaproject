﻿using CinemaProject.Domain.Entities;
using System;

namespace CinemaProject.Application.UseCases.Artists
{
    public class ArtistDto : BaseDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string GenderName { get; set; }        
        public DateTime DateOfBirth { get; set; }
        public int PlaceOfBirthId { get; set; }
        public string PlaceOfBirth { get; set; }
    }
}
