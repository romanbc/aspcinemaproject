﻿using CinemaProject.Application.UseCases.Movies;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Movies
{
    public class MovieValidator : AbstractValidator<MovieDto>
    {
        public MovieValidator(CinemaProjectContext context)
        {
            RuleFor(x => x.Title)
                .NotEmptyAndOfCertainLength();

            RuleFor(x => x.ReleaseDate)
                .NotEmpty()
                .InclusiveBetween(new DateTime(1890, 1, 1), DateTime.UtcNow)
                .WithMessage("Movie could have been filmed over the period of last 130 years");

            RuleFor(x => x.AvailableFrom)
                .NotEmpty()
                .InclusiveBetween(DateTime.UtcNow, DateTime.UtcNow.AddYears(10))
                .WithMessage("Movie could be shown in the next 10 years");

            RuleFor(x => x.AvailableUntill)
                .NotEmpty()
                .InclusiveBetween(DateTime.UtcNow, DateTime.UtcNow.AddYears(10))
                .WithMessage("Movie could be shown in the next 10 years");

            RuleFor(x => x.AvailableFrom)
                .Must((y, x) => x <= y.AvailableUntill)
                .WithMessage("Movie must be available from an earlier date than it is available untill");

            RuleFor(x => x.Runtime)
                .NotEmpty()
                .InclusiveBetween(10, 72 * 60)
                .WithMessage("Movie can not be shorter than 10 minutes or longer than 3 days");

            RuleFor(x => x.ImageFile)
                .Must(x =>
                    new List<string>() { ".jpg", ".jpeg", ".png", ".gif" }
                        .Any(f => f == Path.GetExtension(x.FileName).ToLower())
                )
                .WithMessage("Invalid image format")
                .When(x => x.ImageFile != null);

            RuleFor(x => x.GenreIds)
                .Must(x => x.Count() == x.Distinct().Count())
                .WithMessage("Genres must be unique")
                .When(x => x.GenreIds != null && x.GenreIds.Any());

            RuleForEach(x => x.GenreIds)
                .Must(x => context.Genres.Any(g => g.Id == x))
                .WithMessage("Genre with an id of {PropertyValue} does not exist");

            RuleFor(x => x.Credits)
                .Must(x => x.Count() == x.Distinct(new MovieCreditDtoEqualityComparer()).Count())
                .WithMessage("Artist and artist type combinations must be unique")
                .When(x => x.Credits != null && x.Credits.Any());

            RuleForEach(x => x.Credits)
                .Must(x => context.Artists.Any(a => a.Id == x.ArtistId))
                .WithMessage((y, x) => "Artist with an id of " + x.ArtistId + " does not exist")
                .Must(x => context.ArtistTypes.Any(a => a.Id == x.ArtistTypeId))
                .WithMessage((y, x) => "Artist type with an id of " + x.ArtistTypeId + " does not exist");
        }
    }
}