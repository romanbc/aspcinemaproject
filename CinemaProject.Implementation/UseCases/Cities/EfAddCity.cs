﻿using AutoMapper;
using CinemaProject.Application.UseCases.Cities;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Cities
{
    public class EfAddCity : EfGenericAdd<CityDto, City>, IAddCity
    {
        public EfAddCity(CinemaProjectContext context, IMapper mapper, AddCityValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddCity";
    }
}