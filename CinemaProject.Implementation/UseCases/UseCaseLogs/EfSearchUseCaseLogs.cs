﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.UseCaseLogs;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.UseCaseLogs
{
    public class EfSearchUseCaseLogs : ISearchUseCaseLogs
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;

        public EfSearchUseCaseLogs(CinemaProjectContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public PagedResponse<UseCaseLogDto> Execute(UseCaseLogSearch search)
        {
            var query = _context.UseCaseLogs.AsQueryable();

            if (!string.IsNullOrWhiteSpace(search.Actor))
            {
                query = query.Where(x => x.Actor.ToLower().Contains(search.Actor.ToLower()));
            }

            if (!string.IsNullOrWhiteSpace(search.UseCaseName))
            {
                query = query.Where(x => x.UseCaseName.ToLower().Contains(search.UseCaseName.ToLower()));
            }

            if (search.DateFrom.HasValue)
            {
                query = query.Where(x => x.Date >= search.DateFrom);
            }

            if (search.DateTo.HasValue)
            {
                query = query.Where(x => x.Date <= search.DateTo);
            }

            return query.Paged<UseCaseLogDto, UseCaseLog>(search, _mapper);
        }

        public string Id => "SearchUseCaseLogs";
    }
}