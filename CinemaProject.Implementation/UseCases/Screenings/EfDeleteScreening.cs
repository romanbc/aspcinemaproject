﻿using CinemaProject.Application.UseCases.Screenings;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Screenings
{
    public class EfDeleteScreening : EfGenericDelete<Screening>, IDeleteScreening
    {
        public EfDeleteScreening(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteScreening";

        protected override Func<Screening, bool> CheckForConflicts => x => x.Reservations.Any();
    }
}