﻿namespace CinemaProject.Application.UseCases.Cinemas
{
    public interface ISearchCinemas : IQuery<CinemaSearch, PagedResponse<CinemaDto>>
    {
    }
}