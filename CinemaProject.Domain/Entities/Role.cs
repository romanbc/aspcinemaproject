﻿using CinemaProject.Domain.Relations;
using System.Collections.Generic;

namespace CinemaProject.Domain.Entities
{
    public class Role : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<UserRole> RoleUsers { get; set; } = new HashSet<UserRole>();
        public virtual ICollection<RoleUseCase> RoleUseCases { get; set; } = new HashSet<RoleUseCase>();
    }
}