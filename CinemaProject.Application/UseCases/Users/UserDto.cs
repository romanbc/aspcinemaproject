﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CinemaProject.Application.UseCases.Users
{
    public class UserDto : BaseDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public IEnumerable<int> RoleIds { get; set; }
        public IEnumerable<UserSpecificUseCaseDto> UserSpecificUseCases { get; set; }
    }

    public class ReadUserDto : BaseDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public IEnumerable<string> Roles { get; set; }
        public IEnumerable<string> UseCases { get; set; }
    }

    public class UserSpecificUseCaseDto
    {
        public string UseCaseId { get; set; }
        public bool IsAllowed { get; set; }
    }

    public class UserSpecificUseCaseDtoEqualityComparer : IEqualityComparer<UserSpecificUseCaseDto>
    {
        public bool Equals(UserSpecificUseCaseDto b1, UserSpecificUseCaseDto b2)
        {
            if (b2 == null && b1 == null)
                return true;
            else if (b1 == null || b2 == null)
                return false;
            else if (b1.UseCaseId == b2.UseCaseId)
                return true;
            else
                return false;
        }

        public int GetHashCode([DisallowNull] UserSpecificUseCaseDto obj)
        {
            return obj.UseCaseId.Length.GetHashCode();
        }
    }
}