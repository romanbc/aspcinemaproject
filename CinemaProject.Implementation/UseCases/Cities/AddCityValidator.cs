﻿using CinemaProject.Application.UseCases.Cities;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cities
{
    public class AddCityValidator : AbstractValidator<CityDto>
    {
        public AddCityValidator(CinemaProjectContext context)
        {
            Include(new CityValidator(context));

            RuleFor(x => x.Name)
                .Must((y, x) => !context.Cities.Any(c => c.Name == x && c.CountryId == y.CountryId))
                .WithMessage("Country already has city with the same name.");
        }
    }
}