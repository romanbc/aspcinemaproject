﻿using CinemaProject.Domain.Relations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    public class RoleUseCaseConfiguration : IEntityTypeConfiguration<RoleUseCase>
    {
        public void Configure(EntityTypeBuilder<RoleUseCase> builder)
        {
            builder.HasKey(ruc => new { ruc.RoleId, ruc.UseCaseId });

            builder.Property(ruc => ruc.UseCaseId).IsRequired().HasMaxLength(100);
        }
    }
}