﻿using CinemaProject.Application.UseCases.Seats;
using System;
using System.Collections.Generic;

namespace CinemaProject.Application.UseCases.Screenings
{
    public class ScreeningDto : BaseDto
    {
        public int MovieId { get; set; }
        public int TheaterId { get; set; }
        public DateTime ScreeningStartsAt { get; set; }
        public decimal Price { get; set; }
    }

    public class ReadScreeningDto : BaseDto
    {
        public string Movie { get; set; }
        public string Theater { get; set; }
        public DateTime ScreeningStartsAt { get; set; }
        public decimal Price { get; set; }
        public int MovieRuntime { get; set; }
        public DateTime ScreeningEndsAt => ScreeningStartsAt.AddMinutes(MovieRuntime);
        public IEnumerable<SeatDto> ReservedSeats { get; set; }
        public IEnumerable<SeatDto> AvailableSeats { get; set; }
    }
}