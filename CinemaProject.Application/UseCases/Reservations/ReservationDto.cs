﻿using CinemaProject.Application.UseCases.Seats;
using System.Collections.Generic;

namespace CinemaProject.Application.UseCases.Reservations
{
    public class ReservationDto : BaseDto
    {
        public int ScreeningId { get; set; }
        public int? CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsReserved { get; set; }
        public bool IsPaid { get; set; }
        public IEnumerable<int> SeatIds { get; set; }
        public IEnumerable<SeatDto> ReservedSeats { get; set; }
    }
}