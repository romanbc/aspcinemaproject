﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface IAddReservation : ICommand<ReservationDto>
    {
    }
}