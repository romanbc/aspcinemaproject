﻿using CinemaProject.Domain.Entities;

namespace CinemaProject.Domain.Relations
{
    public class UserUseCase
    {
        public int UserId { get; set; }
        public string UseCaseId { get; set; }
        public bool IsAllowed { get; set; }

        public virtual User User { get; set; }
    }
}