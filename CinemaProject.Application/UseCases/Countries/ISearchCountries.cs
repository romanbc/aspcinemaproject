﻿namespace CinemaProject.Application.UseCases.Countries
{
    public interface ISearchCountries : IQuery<CountrySearch, PagedResponse<CountryDto>>
    {
    }
}