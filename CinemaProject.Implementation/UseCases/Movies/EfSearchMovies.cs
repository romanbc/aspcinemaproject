﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Movies;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Movies
{
    public class EfSearchMovies : EfGenericSearch<Movie, ReadMovieDto, MovieSearch>, ISearchMovies
    {
        private readonly CinemaProjectContext _context;

        public EfSearchMovies(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchMovies";

        public override PagedResponse<ReadMovieDto> Execute(MovieSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Title),
                    PerformFilter = x => x.Where(x =>
                        x.Title.ToLower().Contains(search.Title.ToLower())
                    )
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Genre),
                    PerformFilter = x => x.Where(x =>
                        x.MovieGenres.Any(mg => mg.Genre.Name.ToLower().Contains(search.Genre.ToLower()))
                    )
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Artist),
                    PerformFilter = x => x.Where(x =>
                        x.Credits.Any(x =>
                            x.Artist.FirstName.ToLower().Contains(search.Artist.ToLower())
                            || x.Artist.LastName.ToLower().Contains(search.Artist.ToLower())
                        )
                    )
                },
                new Filter
                {
                    IsConditionMet = () => search.ReleaseDateFrom.HasValue,
                    PerformFilter = x => x.Where(x => x.ReleaseDate >= search.ReleaseDateFrom)
                },
                new Filter
                {
                    IsConditionMet = () => search.ReleaseDateTo.HasValue,
                    PerformFilter = x => x.Where(x => x.ReleaseDate <= search.ReleaseDateTo)
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<Movie>> Query => () => _context.Movies
              .Include(x => x.MovieGenres)
                  .ThenInclude(x => x.Genre)
              .Include(x => x.Credits)
                  .ThenInclude(x => x.Artist)
              .Include(x => x.Credits)
                  .ThenInclude(x => x.ArtistType)
              .AsSplitQuery();
    }
}