﻿namespace CinemaProject.Application.UseCases.Roles
{
    public interface IFindRole : IQuery<int, RoleDto>
    {
    }
}