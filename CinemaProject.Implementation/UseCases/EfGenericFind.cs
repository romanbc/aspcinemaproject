﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases
{
    public abstract class EfGenericFind<TEntity, TDto> : IQuery<int, TDto>
       where TEntity : BaseEntity
       where TDto : BaseDto
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;

        protected EfGenericFind(CinemaProjectContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public abstract string Id { get; }
        public abstract string ChildOf { get; }

        public TDto Execute(int id)
        {
            var item = Query().FirstOrDefault(y => y.Id == id);

            if (item == null)
            {
                throw new EntityNotFoundException(id, typeof(TEntity));
            }

            return _mapper.Map<TDto>(item);
        }

        protected virtual Func<IQueryable<TEntity>> Query => () => _context.Set<TEntity>();
    }
}