﻿namespace CinemaProject.Application.UseCases.Roles
{
    public interface ISearchRoles : IQuery<RoleSearch, PagedResponse<RoleDto>>
    {
    }
}