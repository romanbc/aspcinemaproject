﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Seats;
using Microsoft.AspNetCore.Authorization;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SeatsController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;
        public SeatsController(UseCaseExecutor executor)
        {
            _executor = executor;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get([FromQuery] SeatSearch search, [FromServices] ISearchSeats query)
        {
            return Ok(_executor.ExecuteQuery(query, search));
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromServices] IFindSeat query)
        {
            return Ok(_executor.ExecuteQuery(query, id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] SeatDto dto, [FromServices] IAddSeat command)
        {
            _executor.ExecuteCommand(command, dto);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] SeatDto dto, [FromServices] IUpdateSeat command)
        {
            dto.Id = id;
            _executor.ExecuteCommand(command, dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromServices] IDeleteSeat command)
        {
            _executor.ExecuteCommand(command, id);
            return NoContent();
        }
    }
}
