﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases
{
    public abstract class EfGenericSearch<TEntity, TDto, TSearch> : IQuery<TSearch, PagedResponse<TDto>>
       where TEntity : BaseEntity
       where TDto : BaseDto
       where TSearch : PagedSearch
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;

        protected EfGenericSearch(CinemaProjectContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public abstract string Id { get; }

        public abstract PagedResponse<TDto> Execute(TSearch search);

        protected PagedResponse<TDto> Execute(TSearch search, List<Filter> Filters)
        {
            var query = Query().AsQueryable();

            foreach (Filter filter in Filters)
            {
                if (filter.IsConditionMet())
                {
                    query = filter.PerformFilter(query);
                }
            }

            return query.Paged<TDto, TEntity>(search, _mapper);
        }

        protected virtual Func<IQueryable<TEntity>> Query => () => _context.Set<TEntity>();

        protected class Filter
        {
            public Func<bool> IsConditionMet;
            public Func<IQueryable<TEntity>, IQueryable<TEntity>> PerformFilter;
        }
    }
}