﻿using CinemaProject.Domain.Relations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CinemaProject.Domain.Entities
{
    public class Artist : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }

        [Column(TypeName = "Date")]
        public DateTime DateOfBirth { get; set; }

        public int PlaceOfBirthId { get; set; }

        public virtual City PlaceOfBirth { get; set; }
        public virtual ICollection<Credit> Credits { get; set; } = new HashSet<Credit>();
    }

    public enum Gender
    {
        Male = 1,
        Female,
        Other
    }
}