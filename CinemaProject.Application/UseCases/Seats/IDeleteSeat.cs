﻿namespace CinemaProject.Application.UseCases.Seats
{
    public interface IDeleteSeat : ICommand<int>
    {
    }
}