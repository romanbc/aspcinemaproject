﻿using AutoMapper;
using CinemaProject.Application;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.Extensions
{
    public static class QueryableExtensions
    {
        public static PagedResponse<TDto> Paged<TDto, TSource>
           (this IQueryable<TSource> query, PagedSearch search, IMapper mapper) where TDto : class
        {
            var skip = (search.Page - 1) * search.PerPage;

            return new PagedResponse<TDto>()
            {
                TotalCount = query.Count(),
                ItemsPerPage = search.PerPage,
                CurrentPage = search.Page,
                Items = mapper.Map<IEnumerable<TDto>>(query.Skip(skip).Take(search.PerPage).ToList())
            };
        }
    }
}