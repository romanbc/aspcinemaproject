﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases
{
    public abstract class EfGenericAdd<TDto, TEntity> : ICommand<TDto>
        where TEntity : BaseEntity
        where TDto : BaseDto
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<TDto> _validator;

        protected EfGenericAdd(CinemaProjectContext context, IMapper mapper, AbstractValidator<TDto> validator)
        {
            _context = context;
            _mapper = mapper;
            _validator = validator;
        }

        public abstract string Id { get; }

        public void Execute(TDto request)
        {
            request.Id = 0;
            _validator.ValidateAndThrow(request);
            _context.Set<TEntity>().Add(_mapper.Map<TDto, TEntity>(request));
            _context.SaveChanges();
        }
    }
}