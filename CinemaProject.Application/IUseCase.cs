﻿namespace CinemaProject.Application
{
    public interface ICommand<TRequest> : IUseCase
    {
        void Execute(TRequest request);
    }

    public interface IQuery<TSearch, TResult> : IUseCase
    {
        TResult Execute(TSearch search);
    }

    public interface IUseCase
    {
        string Id { get; }
        string ChildOf => null;
        string Name => Id.ConvertPascalToSentenceCase();
    }
}