﻿using CinemaProject.Application.UseCases.Users;
using CinemaProject.EfDataAccess;
using FluentValidation;

namespace CinemaProject.Implementation.UseCases.Users
{
    public class AddUserValidator : AbstractValidator<UserDto>
    {
        public AddUserValidator(CinemaProjectContext context)
        {
            Include(new UserValidator(context));

            RuleFor(x => x.Password)
                .NotEmpty()
                .MinimumLength(6);
        }
    }
}