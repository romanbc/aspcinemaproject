﻿using AutoMapper;
using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Cinemas
{
    public class EfUpdateCinema : EfGenericUpdate<CinemaDto, Cinema>, IUpdateCinema
    {
        public EfUpdateCinema(CinemaProjectContext context, IMapper mapper, UpdateCinemaValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateCinema";
    }
}