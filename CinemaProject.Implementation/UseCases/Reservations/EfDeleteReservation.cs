﻿using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfDeleteReservation : IDeleteReservation
    {
        private readonly CinemaProjectContext _context;
        private readonly IApplicationActor _actor;

        public EfDeleteReservation(CinemaProjectContext context, IApplicationActor actor)
        {
            _context = context;
            _actor = actor;
        }

        public string Id => "DeleteReservation";

        public void Execute(int id)
        {
            var item = _context.Reservations.FirstOrDefault(x => x.Id == id && x.CustomerId == _actor.Id);

            if (item == null)
            {
                throw new EntityNotFoundException(id, typeof(Reservation));
            }

            item.DeletedAt = DateTime.UtcNow;
            item.ReservedSeats.Clear();
            _context.SaveChanges();
        }
    }
}