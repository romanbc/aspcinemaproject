﻿using AutoMapper;
using CinemaProject.Application.UseCases.Theaters;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Theaters
{
    public class EfFindTheater : EfGenericFind<Theater, TheaterDto>, IFindTheater
    {
        private readonly CinemaProjectContext _context;

        public EfFindTheater(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "FindTheater";

        public override string ChildOf => "SearchTheaters";

        protected override Func<IQueryable<Theater>> Query => () => _context.Theaters.Include(x => x.Cinema).ThenInclude(x => x.City).ThenInclude(x => x.Country).Include(x => x.Seats);
    }
}