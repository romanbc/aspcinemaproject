﻿using AutoMapper;
using CinemaProject.Application.UseCases.Movies;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Movies
{
    public class EfAddMovie : EfGenericAdd<MovieDto, Movie>, IAddMovie
    {
        public EfAddMovie(CinemaProjectContext context, IMapper mapper, AddMovieValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddMovie";
    }
}