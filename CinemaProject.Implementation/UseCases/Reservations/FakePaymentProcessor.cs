﻿using CinemaProject.Application.UseCases.Reservations;
using System;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class FakePaymentProcessor : IPaymentProcessor
    {
        public bool AreTicketsPaid(int customerId, int screeningId, DateTime timestamp)
        {
            //return new Random().NextDouble() >= 0.5;
            return true;
        }
    }
}