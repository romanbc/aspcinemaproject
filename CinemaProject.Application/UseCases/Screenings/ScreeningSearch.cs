﻿using System;

namespace CinemaProject.Application.UseCases.Screenings
{
    public class ScreeningSearch : PagedSearch
    {
        public string Movie { get; set; }
        public string Theater { get; set; }
        public string Cinema { get; set; }
        public int? MovieId { get; set; }
        public int? TheaterId { get; set; }
        public int? CinemaId { get; set; }
        public DateTime? ScreeningStartsAtFrom { get; set; }
        public DateTime? ScreeningStartsAtTo { get; set; }
    }
}