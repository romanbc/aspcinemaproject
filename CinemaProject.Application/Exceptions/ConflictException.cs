﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaProject.Application.Exceptions
{
    public class ConflictException : Exception
    {
        public ConflictException(int id) 
            : base($"Entity with an id of {id} can't be deleted because of existing dependent entities")
        {
        }
    }
}
