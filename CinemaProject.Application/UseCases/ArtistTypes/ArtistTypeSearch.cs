﻿namespace CinemaProject.Application.UseCases.ArtistTypes
{
    public class ArtistTypeSearch : PagedSearch
    {
        public string Name { get; set; }
    }
}