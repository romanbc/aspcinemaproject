﻿using System.Collections.Generic;

namespace CinemaProject.Application.UseCases.Genres
{
    public class GenreDto : BaseDto
    {
        public string Name { get; set; }
        public IEnumerable<string> Movies { get; set; }
    }
}