﻿namespace CinemaProject.Application.UseCases.Roles
{
    public interface IAddRole : ICommand<RoleDto>
    {
    }
}