﻿using CinemaProject.Domain.Relations;
using System.Collections.Generic;

namespace CinemaProject.Domain.Entities
{
    public class Reservation : BaseEntity
    {
        public int ScreeningId { get; set; }
        public int? CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool IsReserved { get; set; }
        public bool IsPaid { get; set; }

        public virtual Screening Screening { get; set; }
        public virtual User Customer { get; set; }

        public virtual ICollection<ReservedSeat> ReservedSeats { get; set; } = new HashSet<ReservedSeat>();
    }
}