﻿using System.Collections.Generic;

namespace CinemaProject.Application.UseCases.Cinemas
{
    public class CinemaDto : BaseDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public int CityId { get; set; }
        public string City { get; set; }
        public IEnumerable<string> Theaters { get; set; }
    }
}