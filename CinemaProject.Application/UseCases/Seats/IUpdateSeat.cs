﻿namespace CinemaProject.Application.UseCases.Seats
{
    public interface IUpdateSeat : ICommand<SeatDto>
    {
    }
}