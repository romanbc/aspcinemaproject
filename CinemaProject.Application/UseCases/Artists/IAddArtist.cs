﻿namespace CinemaProject.Application.UseCases.Artists
{
    public interface IAddArtist : ICommand<ArtistDto>
    {
    }
}
