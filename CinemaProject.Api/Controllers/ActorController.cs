﻿using CinemaProject.Application;
using Microsoft.AspNetCore.Mvc;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActorController : ControllerBase
    {
        private readonly IApplicationActor _actor;

        public ActorController(IApplicationActor actor)
        {
            _actor = actor;
        }

        [HttpGet]
        public ActionResult GetActor()
        {
            return Ok(_actor);
        }
    }
}
