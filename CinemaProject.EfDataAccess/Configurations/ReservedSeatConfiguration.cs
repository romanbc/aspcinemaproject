﻿using CinemaProject.Domain.Relations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    public class ReservedSeatConfiguration : IEntityTypeConfiguration<ReservedSeat>
    {
        public void Configure(EntityTypeBuilder<ReservedSeat> builder)
        {
            builder.HasKey(rs => new { rs.SeatId, rs.ReservationId, rs.ScreeningId });

            builder.HasOne(rs => rs.Seat)
                .WithMany()
                .HasForeignKey(rs => rs.SeatId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(rs => rs.Reservation)
                .WithMany(r => r.ReservedSeats)
                .HasForeignKey(rs => rs.ReservationId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(rs => rs.Screening)
                .WithMany(s => s.ReservedSeats)
                .HasForeignKey(rs => rs.ScreeningId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}