﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    public class CinemaConfiguration : IEntityTypeConfiguration<Cinema>
    {
        public void Configure(EntityTypeBuilder<Cinema> builder)
        {
            //builder.HasIndex(c => c.Name).IsUnique();
            //builder.HasIndex(c => new { c.Name, c.CityId }).IsUnique();
            builder.Property(c => c.Name).IsRequired().HasMaxLength(100);

            builder.Property(c => c.Address).IsRequired().HasMaxLength(100);

            builder.Property(c => c.PhoneNumber).HasMaxLength(50);

            builder.HasOne(c => c.City)
               .WithMany()
               .HasForeignKey(c => c.CityId)
               .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(c => c.Theaters)
                .WithOne(t => t.Cinema)
                .HasForeignKey(t => t.CinemaId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}