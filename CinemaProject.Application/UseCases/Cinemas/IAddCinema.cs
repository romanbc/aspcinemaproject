﻿namespace CinemaProject.Application.UseCases.Cinemas
{
    public interface IAddCinema : ICommand<CinemaDto>
    {
    }
}