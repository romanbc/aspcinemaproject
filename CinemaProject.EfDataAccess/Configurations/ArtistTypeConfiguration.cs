﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    internal class ArtistTypeConfiguration : IEntityTypeConfiguration<ArtistType>
    {
        public void Configure(EntityTypeBuilder<ArtistType> builder)
        {
            //builder.HasIndex(at => at.Name).IsUnique();
            builder.Property(at => at.Name).IsRequired().HasMaxLength(100);
        }
    }
}