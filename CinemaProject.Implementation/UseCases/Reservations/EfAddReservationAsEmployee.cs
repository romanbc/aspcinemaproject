﻿using AutoMapper;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfAddReservationAsEmployee : EfGenericAdd<ReservationDto, Reservation>, IAddReservationAsEmployee
    {
        public EfAddReservationAsEmployee(CinemaProjectContext context, IMapper mapper, AddReservationValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddEmployeeMadeReservation";
    }
}