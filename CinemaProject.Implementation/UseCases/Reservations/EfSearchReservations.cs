﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfSearchReservations : EfGenericSearch<Reservation, ReservationDto, ReservationSearch>, ISearchReservations
    {
        private readonly CinemaProjectContext _context;
        private readonly IApplicationActor _actor;

        public EfSearchReservations(CinemaProjectContext context, IMapper mapper, IApplicationActor actor) : base(context, mapper)
        {
            _context = context;
            _actor = actor;
        }

        public override PagedResponse<ReservationDto> Execute(ReservationSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => true,
                    PerformFilter = x => x.Where(x => x.CustomerId == _actor.Id)
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.FirstName),
                    PerformFilter = x => x.Where(x => x.FirstName.ToLower().Contains(search.FirstName.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.LastName),
                    PerformFilter = x => x.Where(x => x.LastName.ToLower().Contains(search.LastName.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x =>
                        x.FirstName.ToLower().Contains(search.Name.ToLower())
                        || x.LastName.ToLower().Contains(search.Name.ToLower())
                    )
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Email),
                    PerformFilter = x => x.Where(x => x.Email.ToLower().Contains(search.Email.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.PhoneNumber),
                    PerformFilter = x => x.Where(x => x.PhoneNumber.ToLower().Contains(search.PhoneNumber.ToLower()))
                },
                new Filter
                {
                    IsConditionMet = () => search.ScreeningId.HasValue,
                    PerformFilter = x => x.Where(x => x.ScreeningId == search.ScreeningId)
                },
                new Filter
                {
                    IsConditionMet = () => search.CustomerId.HasValue,
                    PerformFilter = x => x.Where(x => x.CustomerId == search.CustomerId)
                },
                new Filter
                {
                    IsConditionMet = () => search.IsPaid.HasValue,
                    PerformFilter = x => x.Where(x => x.IsPaid == search.IsPaid)
                },
                new Filter
                {
                    IsConditionMet = () => search.IsReserved.HasValue,
                    PerformFilter = x => x.Where(x => x.IsReserved == search.IsReserved)
                },
                new Filter
                {
                    IsConditionMet = () => search.SeatId.HasValue,
                    PerformFilter = x => x.Where(x => x.ReservedSeats.Any(rs => rs.SeatId == search.SeatId))
                }
            };

            return Execute(search, filters);
        }

        public override string Id => "SearchReservations";

        protected override Func<IQueryable<Reservation>> Query => () => _context.Reservations.Include(x => x.ReservedSeats).ThenInclude(x => x.Seat);
    }
}