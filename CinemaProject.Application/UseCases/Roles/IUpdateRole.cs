﻿namespace CinemaProject.Application.UseCases.Roles
{
    public interface IUpdateRole : ICommand<RoleDto>
    {
    }
}