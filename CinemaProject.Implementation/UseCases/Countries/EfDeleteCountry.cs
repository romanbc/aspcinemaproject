﻿using CinemaProject.Application.UseCases.Countries;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Countries
{
    public class EfDeleteCountry : EfGenericDelete<Country>, IDeleteCountry
    {
        public EfDeleteCountry(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteCountry";

        protected override Func<Country, bool> CheckForConflicts => x => x.Cities.Any();
    }
}