﻿using AutoMapper;
using CinemaProject.Application.UseCases.Cinemas;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Cinemas
{
    public class EfAddCinema : EfGenericAdd<CinemaDto, Cinema>, IAddCinema
    {
        public EfAddCinema(CinemaProjectContext context, IMapper mapper, AddCinemaValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddCinema";
    }
}