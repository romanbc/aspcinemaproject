﻿using CinemaProject.Implementation.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CinemaProject.Implementation
{
    public static class UseCaseConfig
    {
        private static List<string> UseCaseNamesList { get; set; } = new List<string>();
        public static List<string> SuperAdminExclusiveUseCaseIds => new() {
            "SearchUseCaseLogs",
            "AddRole", 
            "UpdateRole",
            "DeleteRole"
        };

        public static IEnumerable<string> UseCaseDocNames
        {
            get
            {
                if (!UseCaseNamesList.Any())
                {
                    var types = Assembly.GetExecutingAssembly().GetTypes();

                    foreach (var type in types)
                    {
                        if (!type.Name.Contains("Find") && IsOfDesiredType(type))
                        {
                            //Console.WriteLine(type);
                            //Console.WriteLine(type.Name);

                            var numberOfConstructorParams = type.GetConstructors().FirstOrDefault().GetParameters().Length;

                            var instance = Activator.CreateInstance(type, new object[numberOfConstructorParams]);
                            var useCaseId = (string)type.GetProperty("Id").GetValue(instance);

                            UseCaseNamesList.Add(useCaseId);
                        }
                    }
                }

                return UseCaseNamesList;
            }
        }

        private static Func<Type, bool> IsOfDesiredType => type =>
            type.Name != "FakeUseCase"
            && type.IsClass && !type.IsAbstract
            && type.GetInterface("IUseCase") != null
        ;

        public static void AreUseCasesValid(Type[] types = null)
        {
            types ??= Assembly.GetExecutingAssembly().GetTypes();

            var uniqueUseCases = new Dictionary<string, Type>();
            var duplicateUseCaseIds = new HashSet<string>();
            var duplicateUseCaseGroups = new Dictionary<string, List<Type>>();

            var allowedValuesForChildOfIds = UseCaseDocNames;
            var invalidChildOfIds = new Dictionary<Type, string>();

            foreach (var type in types)
            {
                if (IsOfDesiredType(type))
                {
                    var numberOfConstructorParams = type.GetConstructors().FirstOrDefault().GetParameters().Length;

                    var instance = Activator.CreateInstance(type, new object[numberOfConstructorParams]);

                    CheckForDuplicates(type, instance);
                    CheckForInvalidChildOfPropertyValues(type, instance);
                }
            }

            if (invalidChildOfIds.Any())
            {
                throw new UseCaseChildOfNotFoundException(CreateExceptionMessageForInvalidChildOf());
            }

            if (duplicateUseCaseIds.Any())
            {
                foreach (var useCaseId in duplicateUseCaseIds)
                {
                    duplicateUseCaseGroups[useCaseId].Add(uniqueUseCases[useCaseId]);
                }

                throw new UseCaseIdNotUniqueException(CreateExceptionMessageForDuplicates());
            }

            void CheckForDuplicates(Type type, object instance)
            {
                var useCaseId = type.GetProperty("Id").GetValue(instance).ToString().Trim().ToUpper();

                if (!uniqueUseCases.ContainsKey(useCaseId))
                {
                    uniqueUseCases.Add(useCaseId, type);
                }
                else
                {
                    duplicateUseCaseIds.Add(useCaseId);

                    if (duplicateUseCaseGroups.ContainsKey(useCaseId))
                    {
                        duplicateUseCaseGroups[useCaseId].Add(type);
                    }
                    else
                    {
                        duplicateUseCaseGroups[useCaseId] = new List<Type> { type };
                    }
                }
            }

            void CheckForInvalidChildOfPropertyValues(Type type, object instance)
            {
                var childOfProperty = type.GetInterface("IUseCase").GetProperty("ChildOf");

                if (childOfProperty.GetMethod.IsVirtual && childOfProperty.GetValue(instance) != null)
                {
                    var childOfValue = childOfProperty.GetValue(instance).ToString();

                    if (!allowedValuesForChildOfIds.Contains(childOfValue))
                    {
                        invalidChildOfIds.Add(type, childOfValue);
                    }
                }
            }

            string CreateExceptionMessageForInvalidChildOf()
            {
                string message = Environment.NewLine;

                foreach (var item in invalidChildOfIds)
                {
                    message += $"{Environment.NewLine}Nonexistent use case id '{item.Value}' is used for ChildOf property in {item.Key}{Environment.NewLine}";
                }

                return message;
            }

            string CreateExceptionMessageForDuplicates()
            {
                StringBuilder sb = new();

                sb.AppendLine();
                sb.AppendLine("Duplicate use case ids found.");

                foreach (var useCaseGroup in duplicateUseCaseGroups)
                {
                    sb.AppendLine();
                    sb.AppendFormat("Same Id '{0}' appears in {1} use cases:", useCaseGroup.Key, useCaseGroup.Value.Count);
                    sb.AppendLine();

                    foreach (var useCase in useCaseGroup.Value)
                    {
                        sb.Append(useCase);
                        sb.AppendLine();
                    }
                }

                return sb.ToString();
            }
        }
    }
}