﻿namespace CinemaProject.Application.UseCases.Genres
{
    public interface IDeleteGenre : ICommand<int>
    {
    }
}