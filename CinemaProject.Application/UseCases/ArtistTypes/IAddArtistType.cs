﻿namespace CinemaProject.Application.UseCases.ArtistTypes
{
    public interface IAddArtistType : ICommand<ArtistTypeDto>
    {
    }
}