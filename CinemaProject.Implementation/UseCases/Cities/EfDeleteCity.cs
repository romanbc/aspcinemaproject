﻿using CinemaProject.Application.UseCases.Cities;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Cities
{
    public class EfDeleteCity : EfGenericDelete<City>, IDeleteCity
    {
        private readonly CinemaProjectContext _context;

        public EfDeleteCity(CinemaProjectContext context) : base(context)
        {
            _context = context;
        }

        public override string Id => "DeleteCity";

        protected override Func<City, bool> CheckForConflicts => x =>
              _context.Artists.Any(a => a.PlaceOfBirthId == x.Id)
              || _context.Cinemas.Any(a => a.CityId == x.Id)
        ;
    }
}