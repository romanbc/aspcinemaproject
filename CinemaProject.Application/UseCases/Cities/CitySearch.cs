﻿namespace CinemaProject.Application.UseCases.Cities
{
    public class CitySearch : PagedSearch
    {
        public string Name { get; set; }
        public string Country { get; set; }
    }
}