﻿using CinemaProject.Application.UseCases.Users;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using CinemaProject.Implementation.Extensions;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Users
{
    public class UserValidator : AbstractValidator<UserDto>
    {
        public UserValidator(CinemaProjectContext context, bool isUpdate = false)
        {
            RuleFor(x => x.FirstName)
                .ConsistsOfLettersDashesAndSpaces()
                .When(x => !string.IsNullOrWhiteSpace(x.FirstName));

            RuleFor(x => x.LastName)
                .ConsistsOfLettersDashesAndSpaces()
                .When(x => !string.IsNullOrWhiteSpace(x.LastName));

            RuleFor(x => x.Email)
                .NotEmpty()
                .EmailAddress()
                .MaximumLength(100)
                .CheckIfAlreadyExists<UserDto, User>(context, "Email", isUpdate);

            RuleFor(x => x.PhoneNumber)
               .PhoneNumberValidation()
               .When(x => !string.IsNullOrWhiteSpace(x.PhoneNumber));

            RuleFor(x => x.RoleIds)
               .Must(x => x.Count() == x.Distinct().Count())
               .WithMessage("Roles must be unique")
               .When(x => x.RoleIds != null && x.RoleIds.Any());

            RuleForEach(x => x.RoleIds)
                .Must(x => context.Roles.Any(r => r.Id == x))
                .WithMessage("Role with an id of {PropertyValue} does not exist");

            RuleFor(x => x.UserSpecificUseCases)
                .Must(x => x.All(usuc => !string.IsNullOrWhiteSpace(usuc.UseCaseId)))
                .WithMessage("Use case id is a mandatory field")
                .Must(x => x.Count() == x.Distinct(new UserSpecificUseCaseDtoEqualityComparer()).Count())
                .WithMessage("Use cases must be unique")
                .When(x => x.UserSpecificUseCases != null && x.UserSpecificUseCases.Any());

            RuleForEach(x => x.UserSpecificUseCases)
                .Must(x => UseCaseConfig.UseCaseDocNames.Contains(x.UseCaseId))
                .WithMessage((y, x) => "Use case " + x.UseCaseId + " does not exist");
        }
    }
}