﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    public class TheaterConfiguration : IEntityTypeConfiguration<Theater>
    {
        public void Configure(EntityTypeBuilder<Theater> builder)
        {
            //builder.HasIndex(t => new { t.Name, t.CinemaId }).IsUnique();

            builder.Property(t => t.Name).IsRequired().HasMaxLength(100);

            builder.HasMany(t => t.Seats)
                .WithOne(s => s.Theater)
                .HasForeignKey(s => s.TheaterId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}