﻿using CinemaProject.Domain.Relations;
using System.Collections.Generic;

namespace CinemaProject.Domain.Entities
{
    public class ArtistType : BaseEntity
    {
        public string Name { get; set; }

        public virtual ICollection<Credit> Credits { get; set; } = new HashSet<Credit>();
    }
}