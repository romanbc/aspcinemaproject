﻿using AutoMapper;
using CinemaProject.Application.UseCases.Roles;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Roles
{
    public class EfAddRole : EfGenericAdd<RoleDto, Role>, IAddRole
    {
        public EfAddRole(CinemaProjectContext context, IMapper mapper, AddRoleValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddRole";
    }
}