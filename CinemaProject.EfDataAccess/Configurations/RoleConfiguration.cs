﻿using CinemaProject.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CinemaProject.EfDataAccess.Configurations
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.Property(r => r.Name).IsRequired().HasMaxLength(100);
            //builder.HasIndex(r => r.Name).IsUnique();

            builder.HasMany(r => r.RoleUseCases)
                .WithOne(ruc => ruc.Role)
                .HasForeignKey(ruc => ruc.RoleId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}