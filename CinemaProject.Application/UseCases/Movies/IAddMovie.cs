﻿namespace CinemaProject.Application.UseCases.Movies
{
    public interface IAddMovie : ICommand<MovieDto>
    {
    }
}