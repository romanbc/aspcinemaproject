﻿using CinemaProject.Application.UseCases.Seats;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Seats
{
    public class EfDeleteSeat : EfGenericDelete<Seat>, IDeleteSeat
    {
        private readonly CinemaProjectContext _context;

        public EfDeleteSeat(CinemaProjectContext context) : base(context)
        {
            _context = context;
        }

        public override string Id => "DeleteSeat";

        protected override Func<Seat, bool> CheckForConflicts => x => _context.ReservedSeats.Any(rs => rs.SeatId == x.Id);
    }
}