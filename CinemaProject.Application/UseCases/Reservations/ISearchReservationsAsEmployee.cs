﻿namespace CinemaProject.Application.UseCases.Reservations
{
    public interface ISearchReservationsAsEmployee : IQuery<ReservationSearch, PagedResponse<ReservationDto>>
    {
    }
}