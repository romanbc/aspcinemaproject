﻿using AutoMapper;
using CinemaProject.Application.UseCases.Movies;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Movies
{
    public class EfUpdateMovie : EfGenericUpdate<MovieDto, Movie>, IUpdateMovie
    {
        public EfUpdateMovie(CinemaProjectContext context, IMapper mapper, UpdateMovieValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateMovie";
    }
}