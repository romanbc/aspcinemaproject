﻿using CinemaProject.Application.UseCases.Email;

namespace CinemaProject.Implementation.UseCases.Email
{
    public class EfSendEmail : ISendEmail
    {
        private readonly IEmailSender _sender;

        public EfSendEmail(IEmailSender sender)
        {
            _sender = sender;
        }

        public void Execute(SendEmailDto request)
        {
            _sender.Send(request);
        }

        public string Id => "SendEmail";
    }
}