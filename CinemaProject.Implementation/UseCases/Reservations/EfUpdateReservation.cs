﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfUpdateReservation : IUpdateReservation
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;
        private readonly UpdateReservationValidator _validator;
        private readonly IApplicationActor _actor;
        private readonly IPaymentProcessor _paymentProcessor;

        public EfUpdateReservation(CinemaProjectContext context, IMapper mapper, UpdateReservationValidator validator, IApplicationActor actor, IPaymentProcessor paymentProcessor)
        {
            _context = context;
            _mapper = mapper;
            _validator = validator;
            _actor = actor;
            _paymentProcessor = paymentProcessor;
        }

        public void Execute(ReservationDto request)
        {
            request.CustomerId = _actor.Id;

            var reservation = _context.Reservations.FirstOrDefault(x => x.Id == request.Id && x.CustomerId == request.CustomerId);

            var user = _context.Users.Find(request.CustomerId);

            if (request.IsPaid)
            {
                if (!_paymentProcessor.AreTicketsPaid((int)request.CustomerId, request.ScreeningId, DateTime.UtcNow))
                {
                    throw new UnauthorizedUseCaseException(this, _actor);
                }
            }

            if (reservation == null)
            {
                throw new EntityNotFoundException(request.Id, typeof(Reservation));
            }

            request.FirstName = user.FirstName;
            request.LastName = user.LastName;
            request.PhoneNumber = user.PhoneNumber;
            request.Email = user.Email;

            _validator.ValidateAndThrow(request);
            _mapper.Map(request, reservation);

            _context.SaveChanges();
        }

        public string Id => "UpdateReservation";
    }
}