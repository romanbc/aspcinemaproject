﻿namespace CinemaProject.Application.UseCases.Genres
{
    public interface IAddGenre : ICommand<GenreDto>
    {
    }
}