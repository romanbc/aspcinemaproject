﻿using CinemaProject.Application.UseCases.Movies;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Movies
{
    public class EfDeleteMovie : EfGenericDelete<Movie>, IDeleteMovie
    {
        public EfDeleteMovie(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteMovie";

        protected override Func<Movie, bool> CheckForConflicts => x => x.Screenings.Any();
    }
}