﻿using System.Collections.Generic;

namespace CinemaProject.Application
{
    public interface IApplicationActor
    {
        int Id { get; }
        string Identity { get; }
        bool IsSuperAdmin { get; }
        IEnumerable<string> AllowedUseCases { get; }
    }
}