﻿using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Application.UseCases.Users;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Users
{
    public class EfDeleteUser : IDeleteUser
    {
        private readonly CinemaProjectContext _context;
        private readonly IApplicationActor _actor;

        public EfDeleteUser(CinemaProjectContext context, IApplicationActor actor)
        {
            _context = context;
            _actor = actor;
        }

        public void Execute(int id)
        {
            var user = _context.Users.Find(id);

            if (user == null)
            {
                throw new EntityNotFoundException(id, typeof(User));
            }

            if (user.Reservations.Any())
            {
                throw new ConflictException(id);
            }
            
            if (!_actor.IsSuperAdmin && user.UserRoles.Any(x => x.Role.Name == "SuperAdmin"))
            {
                throw new UnauthorizedUseCaseException(this, _actor);
            }

            user.DeletedAt = DateTime.UtcNow;
            _context.SaveChanges();
        }

        public string Id => "DeleteUser";
    }
}