﻿using AutoMapper;
using CinemaProject.Application.UseCases.Reservations;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Reservations
{
    public class EfUpdateReservationAsEmployee : EfGenericUpdate<ReservationDto, Reservation>, IUpdateReservationAsEmployee
    {
        public EfUpdateReservationAsEmployee(CinemaProjectContext context, IMapper mapper, UpdateReservationValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateEmployeeMadeReservation";
    }
}