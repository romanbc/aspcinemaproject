﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.ArtistTypes;
using Microsoft.AspNetCore.Authorization;

namespace CinemaProject.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArtistTypesController : ControllerBase
    {
        private readonly UseCaseExecutor _executor;
        public ArtistTypesController(UseCaseExecutor executor)
        {
            _executor = executor;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get([FromQuery] ArtistTypeSearch search, [FromServices] ISearchArtistTypes query)
        {
            return Ok(_executor.ExecuteQuery(query, search));
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult Get(int id, [FromServices] IFindArtistType query)
        {
            return Ok(_executor.ExecuteQuery(query, id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] ArtistTypeDto dto, [FromServices] IAddArtistType command)
        {
            _executor.ExecuteCommand(command, dto);
            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ArtistTypeDto dto, [FromServices] IUpdateArtistType command)
        {
            dto.Id = id;
            _executor.ExecuteCommand(command, dto);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromServices] IDeleteArtistType command)
        {
            _executor.ExecuteCommand(command, id);
            return NoContent();
        }
    }
}
