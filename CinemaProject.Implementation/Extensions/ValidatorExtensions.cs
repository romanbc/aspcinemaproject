﻿using CinemaProject.Application;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using FluentValidation;
using FluentValidation.Validators;
using System;
using System.Linq;

namespace CinemaProject.Implementation.Extensions
{
    public static class ValidatorExtensions
    {
        public static IRuleBuilderOptions<T, string> TitleValidation<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .NotEmptyAndOfCertainLength()
                .StringIsInDesiredFormatForTitle();
        }

        public static IRuleBuilderOptions<T, string> NameValidation<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .NotEmptyAndOfCertainLength()
                .ConsistsOfLettersDashesAndSpaces();
        }

        public static IRuleBuilderOptions<T, string> PhoneNumberValidation<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Matches(@"^(\+|00)?[0-9][0-9\/ \-]{3,30}[0-9]+$")
                .WithMessage("{PropertyName} is not a valid phone number");
        }

        public static IRuleBuilderOptions<T, string> NotEmptyAndOfCertainLength<T>(this IRuleBuilder<T, string> ruleBuilder, int from = 2, int to = 100)
        {
            return ruleBuilder
                .NotEmpty()
                .WithMessage("{PropertyName} is required.")
                .Length(2, 100)
                .WithMessage("{PropertyName} must be between " + from + " and " + to + " characters.");
        }

        public static IRuleBuilderOptions<T, string> StringIsInDesiredFormatForTitle<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Must(
                    x => Char.IsLetter(x.First()) && x[1..]
                        .All(c => Char.IsLetterOrDigit(c) || c == '_' || c == '-' || c == '.' || c == ' ')
                )
                .WithMessage("{PropertyName} can only contain alphanumeric characters, underscores, dashes, periods and spaces and must start with a letter.");
        }

        public static IRuleBuilderOptions<T, string> ConsistsOfLettersDashesAndSpaces<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .Matches(@"^\p{L}[\p{L}- ]*\p{L}$")
                .WithMessage("{PropertyName} can only contain letters, dashes and spaces and must start and end with a letter.");
        }

        public static IRuleBuilderOptions<T, string> CheckIfAlreadyExists<T, TEntity>(this IRuleBuilder<T, string> ruleBuilder, CinemaProjectContext context, string propertyName, bool isUpdate = false)
        where T : BaseDto
        where TEntity : BaseEntity
        {
            if (isUpdate)
            {
                //bool predicate(T instance, string propertyValue, ValidationContext<T> propertyValidatorContext) =>
                //  !context.Set<TEntity>().IgnoreQueryFilters().AsEnumerable().Any(x =>
                //      x.GetType().GetProperty(propertyName).GetValue(x).ToString() == propertyValue.ToString()
                //      && x.Id != instance.Id
                //  );

                bool predicate(T instance, string propertyValue, ValidationContext<T> propertyValidatorContext) =>
                  !context.Set<TEntity>().AsEnumerable().Any(x =>
                      x.GetType().GetProperty(propertyName).GetValue(x).ToString() == propertyValue.ToString()
                      && x.Id != instance.Id
                  );

                return ruleBuilder.SetValidator(new DoesNotAlreadyExistValidator<T, string>(
                    (instance, propertyValue, propertyValidatorContext) =>
                        predicate(instance, propertyValue, propertyValidatorContext))
                );
            }
            else
            {
                //bool predicate(string propertyValue, ValidationContext<T> propertyValidatorContext) =>
                //    !context.Set<TEntity>().IgnoreQueryFilters().AsEnumerable().Any(x =>
                //        x.GetType().GetProperty(propertyName).GetValue(x).ToString() == propertyValue.ToString()
                //    );

                bool predicate(string propertyValue, ValidationContext<T> propertyValidatorContext) =>
                   !context.Set<TEntity>().AsEnumerable().Any(x =>
                       x.GetType().GetProperty(propertyName).GetValue(x).ToString() == propertyValue.ToString()
                   );

                return ruleBuilder.SetValidator(new DoesNotAlreadyExistValidator<T, string>(
                    (instance, propertyValue, propertyValidatorContext) =>
                        predicate(propertyValue, propertyValidatorContext))
                );
            }
        }
    }

    public class DoesNotAlreadyExistValidator<T, TProperty> : PropertyValidator<T, TProperty>, IPropertyValidator
    {
        public delegate bool Predicate(T instanceToValidate, TProperty propertyValue, ValidationContext<T> propertyValidatorContext);

        private readonly Predicate _predicate;

        public DoesNotAlreadyExistValidator(Predicate predicate) =>
            _predicate = predicate;

        public override bool IsValid(ValidationContext<T> context, TProperty value) =>
            _predicate(context.InstanceToValidate, value, context);

        public override string Name => "DoesNotAlreadyExist";

        protected override string GetDefaultMessageTemplate(string errorCode) =>
            "{PropertyName} is already taken.";
    }
}