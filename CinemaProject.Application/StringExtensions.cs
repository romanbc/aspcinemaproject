﻿using System.Text.RegularExpressions;

namespace CinemaProject.Application
{
    public static class StringExtensions
    {
        public static string ConvertPascalToSentenceCase(this string str)
        {
            string[] split = Regex.Split(str, @"(?<!^)(?=[A-Z])");

            return string.Join(" ", split);
        }
    }
}