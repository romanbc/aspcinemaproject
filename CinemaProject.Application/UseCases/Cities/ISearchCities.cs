﻿namespace CinemaProject.Application.UseCases.Cities
{
    public interface ISearchCities : IQuery<CitySearch, PagedResponse<CityDto>>
    {
    }
}