﻿using System.Collections.Generic;

namespace CinemaProject.Domain.Entities
{
    public class Theater : BaseEntity
    {
        public string Name { get; set; }
        public int CinemaId { get; set; }

        public virtual Cinema Cinema { get; set; }
        public virtual ICollection<Seat> Seats { get; set; } = new HashSet<Seat>();
    }
}