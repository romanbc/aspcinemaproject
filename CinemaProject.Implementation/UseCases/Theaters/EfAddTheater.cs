﻿using AutoMapper;
using CinemaProject.Application.UseCases.Theaters;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Theaters
{
    public class EfAddTheater : EfGenericAdd<TheaterDto, Theater>, IAddTheater
    {
        public EfAddTheater(CinemaProjectContext context, IMapper mapper, AddTheaterValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "AddTheater";
    }
}