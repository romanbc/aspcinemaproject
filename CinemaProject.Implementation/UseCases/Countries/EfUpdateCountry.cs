﻿using AutoMapper;
using CinemaProject.Application.UseCases.Countries;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Countries
{
    public class EfUpdateCountry : EfGenericUpdate<CountryDto, Country>, IUpdateCountry
    {
        public EfUpdateCountry(CinemaProjectContext context, IMapper mapper, UpdateCountryValidator validator) : base(context, mapper, validator)
        {
        }

        public override string Id => "UpdateCountry";
    }
}