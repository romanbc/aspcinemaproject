﻿namespace CinemaProject.Application.UseCases.Artists
{
    public interface IFindArtist : IQuery<int, ArtistDto>
    {
    }
}
