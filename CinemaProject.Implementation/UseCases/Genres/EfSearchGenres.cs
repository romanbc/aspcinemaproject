﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.UseCases.Genres;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Genres
{
    public class EfSearchGenres : EfGenericSearch<Genre, GenreDto, GenreSearch>, ISearchGenres
    {
        private readonly CinemaProjectContext _context;

        public EfSearchGenres(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
            _context = context;
        }

        public override string Id => "SearchGenres";

        public override PagedResponse<GenreDto> Execute(GenreSearch search)
        {
            List<Filter> filters = new()
            {
                new Filter
                {
                    IsConditionMet = () => !string.IsNullOrWhiteSpace(search.Name),
                    PerformFilter = x => x.Where(x => x.Name.ToLower().Contains(search.Name.ToLower()))
                }
            };

            return Execute(search, filters);
        }

        protected override Func<IQueryable<Genre>> Query => () =>
              _context.Genres.Include(x => x.GenreMovies).ThenInclude(x => x.Movie);
    }
}