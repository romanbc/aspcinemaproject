﻿using CinemaProject.Application.UseCases.Roles;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using System;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Roles
{
    public class EfDeleteRole : EfGenericDelete<Role>, IDeleteRole
    {
        public EfDeleteRole(CinemaProjectContext context) : base(context)
        {
        }

        public override string Id => "DeleteRole";

        protected override Func<Role, bool> CheckForConflicts => x => x.RoleUsers.Any();
    }
}