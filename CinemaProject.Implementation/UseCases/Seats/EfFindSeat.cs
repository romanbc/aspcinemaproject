﻿using AutoMapper;
using CinemaProject.Application.UseCases.Seats;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;

namespace CinemaProject.Implementation.UseCases.Seats
{
    public class EfFindSeat : EfGenericFind<Seat, SeatDto>, IFindSeat
    {
        public EfFindSeat(CinemaProjectContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public override string Id => "FindSeat";

        public override string ChildOf => "SearchSeats";
    }
}