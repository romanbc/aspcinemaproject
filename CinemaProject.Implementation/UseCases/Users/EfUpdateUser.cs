﻿using AutoMapper;
using CinemaProject.Application;
using CinemaProject.Application.Exceptions;
using CinemaProject.Application.UseCases.Users;
using CinemaProject.Domain.Entities;
using CinemaProject.EfDataAccess;
using FluentValidation;
using System.Linq;

namespace CinemaProject.Implementation.UseCases.Users
{
    public class EfUpdateUser : IUpdateUser
    {
        private readonly CinemaProjectContext _context;
        private readonly IMapper _mapper;
        private readonly UpdateUserValidator _validator;
        private readonly IApplicationActor _actor;

        public EfUpdateUser(CinemaProjectContext context, IMapper mapper, UpdateUserValidator validator, IApplicationActor actor)
        {
            _context = context;
            _mapper = mapper;
            _validator = validator;
            _actor = actor;
        }

        public void Execute(UserDto request)
        {
            var user = _context.Users.Find(request.Id);

            if (user == null)
            {
                throw new EntityNotFoundException(request.Id, typeof(User));
            }
            
            if (!_actor.IsSuperAdmin)
            {
                if (user.UserRoles.Any(x => x.Role.Name == "SuperAdmin"))
                {
                    throw new UnauthorizedUseCaseException(this, _actor);
                }

                _validator.Include(new CheckForSuperAdminPrivilegesValidator(_context));
            }

            _validator.ValidateAndThrow(request);

            _mapper.Map(request, user);
            _context.SaveChanges();
        }

        public string Id => "UpdateUser";
    }
}