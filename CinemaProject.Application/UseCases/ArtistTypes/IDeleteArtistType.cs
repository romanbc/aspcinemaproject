﻿namespace CinemaProject.Application.UseCases.ArtistTypes
{
    public interface IDeleteArtistType : ICommand<int>
    {
    }
}